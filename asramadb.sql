-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 22 Mei 2014 pada 13.59
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `asramadb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `alamat`
--

CREATE TABLE IF NOT EXISTS `alamat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penghuni_id` int(11) NOT NULL,
  `RT_RW` varchar(45) NOT NULL,
  `kecamatan` varchar(45) NOT NULL,
  `kelurahan` varchar(45) NOT NULL,
  `kota` varchar(45) NOT NULL,
  `kode_pos` varchar(45) NOT NULL,
  `no_telepon` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_alamat_penghuni1` (`penghuni_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data untuk tabel `alamat`
--

INSERT INTO `alamat` (`id`, `penghuni_id`, `RT_RW`, `kecamatan`, `kelurahan`, `kota`, `kode_pos`, `no_telepon`) VALUES
(2, 1, 'aa', 'aa', 'aa', 'aa', 'aa', '113'),
(3, 1, 'www', 'www', 'www', 'www', 'www', '111'),
(4, 1, 'www', 'www', 'www', 'www', 'www', '111'),
(5, 6, 'asdasd', 'asasd', 'asdas', 'adas', 'asd', 'asdas'),
(6, 7, 'rt rw', 'kecamatan', 'kelurahan', 'kota', '1123', '08123432423'),
(7, 1, '-', '-', '-', '-', '76114', '085652039639'),
(8, 9, '', '', '', '', '', ''),
(9, 10, 'jalannya belum ada Ta', 'Gharta, urutannya kelurahan dulu baru kecamat', 'pindahkan posisinya ke atas Ta', 'Bandung', '40132', '01234546'),
(10, 11, '004/002', 'Kragilan', 'Kragilan', 'Serang', '42184', '-'),
(11, 12, '3/7', 'Lubuk Kilangan', 'Indarung', 'Padang', '25237', '085363802084'),
(12, 13, '04/10', 'Bojonggede', 'Kedung Waringin', 'Bogor', '16923', '0251-8554753'),
(13, 14, 'jl. jaksanaranata no 16', 'baleendah', 'baleendah', 'kabupaten bandung', '40375', '-'),
(14, 15, 'Jalan Ciraden RT 10/03', 'Cisaat', 'Cisaat', 'Sukabumi-Jawa Barat', '43152', '085723033349'),
(15, 16, 'jalan ranca bentang no 7 rt 04 rw 15 Cimahi s', 'cimahi selatan', 'cibereum', 'cimahi', '40535', '022-6025707'),
(16, 17, 'RT2/RW4', 'Jatilawang', 'Gentawangi', 'Banyumas', '53174', '0'),
(17, 18, 'RT4/RW14', 'Cepogo', 'Tumang', 'Boyolali', '57311', '0'),
(18, 19, 'Jorong Tanjuang Nagari ', 'X Koto', 'Pandai Sikek', 'Tanah Datar', '00000', '0'),
(19, 20, 'Dusun Nagasaribu no 56', 'pamatang silimahuta', '', 'simalungun', '21167', '081260094255'),
(20, 21, '06/03', 'jaken', 'srikaton', 'pati', '59184', '085643252737'),
(21, 22, '', 'Deli Serdang', 'Sukaluwe', 'Medan', '', ''),
(22, 23, 'Dsn Kebonduren 03/02', 'Ponggok', 'Kebonduren', 'Blitar', '66152', '085793157828'),
(23, 24, '1/4', 'kawalu', 'talagasari', 'Tasikmalaya', '46182', ''),
(24, 25, '4/2', 'Rawalo', '', 'Banyumas', '53173', ''),
(25, 26, 'Link. Leuweung Sawo no.70 rt01/rw09', 'Purwakarta', 'Kotabumi', 'Kota  Cilegon', '42434', '081319733283'),
(26, 27, 'Jl. Gunung Bromo, RT/RW.03/08', 'Indramayu', 'Margadadi', 'Indramayu', '45211', '(0234)273661'),
(27, 28, 'Mekar Indah II, blok B.214, Rt 4/Rw 13 ', 'Cileunyi', 'Cibiruwetan', 'Kabupaten Bandung', '40625', '087825725652'),
(28, 29, 'Jalan Suparjan Mangun WIjaya no 11 D, RT12 RW', 'Mojoroto', 'Mojoroto', 'Kota Kediri', '64112', '-'),
(29, 30, 'Sendangguwo baru IV no 12, RT 07 RW 07', 'Pedurungan', 'Gemah', 'Semarang', '50191', '024-6713937'),
(30, 31, 'Satya 9 No.8 , RT 02 RW 04', 'Pasar Rebo', 'Baru', 'Jakarta Timur', '13780', '02170981116'),
(31, 32, 'kp cilamajang 03/06', 'mangkubumi', 'cipawitra', 'tasikmalaya', '46818', '085795063840'),
(32, 33, 'Jl. Camar no. 3 Perum BEI', 'Cipedes', 'Sukamanh', 'Tasikmalaya', '46131', '085722489717'),
(33, 34, 'Jl Permai no 1', 'Cimenyan', 'Padasuka', 'Bandung', '40192', '085624449859'),
(34, 35, 'Ngadirejo RT05/03', 'Jumantono', 'Ngunut', 'Karanganyar', '57782', '081329749256'),
(35, 36, 'Langsep 6A 03/05', 'Tuban', 'Latsai', 'Tuban', '62314', '0356332786'),
(36, 37, '', '', '', '', '', ' 3c cqr34rqr4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `alokasi`
--

CREATE TABLE IF NOT EXISTS `alokasi` (
  `kode` varchar(45) NOT NULL,
  `penghuni_id` int(11) NOT NULL,
  `kamar_nomor_kamar` varchar(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_aktif` int(11) NOT NULL,
  PRIMARY KEY (`kode`),
  KEY `fk_pendaftaran_asrama_penghuni1` (`penghuni_id`),
  KEY `fk_pendaftaran_asrama_kamar1` (`kamar_nomor_kamar`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `alokasi`
--

INSERT INTO `alokasi` (`kode`, `penghuni_id`, `kamar_nomor_kamar`, `tanggal`, `status_aktif`) VALUES
('KODE2_2013-05-14 05:49:23', 2, 'A01_KN', '2013-05-15 13:13:07', 0),
('KODE3_2013-05-14 04:25:44', 3, 'B01_KP', '2013-05-14 15:46:00', 0),
('KODE3_2013-05-15 04:12:10', 3, 'B01_KP', '2013-05-30 11:23:53', 0),
('KODE4_2013-05-14 04:25:49', 4, 'C04_KP', '2013-05-17 19:09:49', 0),
('KODE4_2013-05-14 04:41:57', 4, 'C04_KP', '2013-05-17 19:09:49', 0),
('KODE4_2013-05-15 04:26:13', 4, 'C04_KP', '2013-05-30 11:23:58', 0),
('KODE4_2013-05-30 02:36:34', 4, 'B01_KP', '2013-05-30 18:09:14', 0),
('KODE5_2013-05-14 04:25:52', 5, 'B01_KP', '2013-05-14 14:52:09', 0),
('KODE5_2013-05-15 04:12:20', 5, 'C02_KP', '2013-05-15 13:12:25', 0),
('KODE5_2013-05-15 08:35:55', 5, 'B07_KP', '2013-05-15 18:16:30', 0),
('KODE5_2013-05-17 08:26:57', 5, 'B03_KP', '2013-05-17 19:08:48', 0),
('KODE6_2013-05-14 04:25:28', 6, 'B01_KP', '2013-05-17 19:06:46', 0),
('KODE6_2013-05-14 04:45:45', 6, 'B01_KP', '2013-05-17 19:06:46', 0),
('KODE6_2013-05-14 04:49:53', 6, 'B01_KP', '2013-05-17 19:06:46', 0),
('KODE6_2013-05-15 09:15:20', 6, 'B01_KP', '2013-05-30 11:22:52', 0),
('KODE7_2013-05-14 05:49:08', 7, 'A01_KN', '2013-05-17 19:04:13', 0),
('KODE7_2013-05-17 10:06:21', 7, 'A07_KN', '2013-05-30 11:24:24', 0),
('KODE8_2013-05-31 02:06:49', 8, 'C15_KP', '2013-06-01 07:58:42', 0),
('KODE14_2013-05-31 02:07:52', 14, 'C01_KN', '2013-05-31 06:07:51', 1),
('KODE19_2013-05-31 02:08:22', 19, 'C01_KN', '2013-05-31 06:08:22', 1),
('KODE15_2013-05-31 02:08:47', 15, 'A10_KN', '2013-06-02 15:32:10', 0),
('KODE13_2013-05-31 02:09:12', 13, 'K26_KN', '2013-05-31 06:09:11', 1),
('KODE11_2013-05-31 02:16:28', 11, 'C01_KP', '2013-05-31 07:36:06', 0),
('KODE12_2013-05-31 02:16:55', 12, 'E01_KP', '2013-05-31 06:16:54', 1),
('KODE17_2013-05-31 02:17:37', 17, 'B01_KP', '2013-06-05 06:49:10', 1),
('KODE20_2013-05-31 02:18:07', 20, 'B01_KP', '2013-05-31 08:20:31', 0),
('KODE22_2013-05-31 03:00:01', 22, 'C01_KP', '2013-06-01 09:58:58', 1),
('KODE23_2013-05-31 03:00:36', 23, 'E01_KP', '2013-06-02 15:29:57', 0),
('KODE8_2013-05-31 03:05:57', 8, 'C15_KP', '2013-06-01 07:58:42', 0),
('KODE25_2013-05-31 03:28:16', 25, 'F01_KP', '2013-06-02 15:29:12', 0),
('KODE16_2013-05-31 03:35:00', 16, 'B08_KP', '2013-05-31 07:35:35', 0),
('KODE11_2013-05-31 03:36:21', 11, 'C01_KP', '2013-05-31 08:15:55', 0),
('KODE18_2013-05-31 03:36:47', 18, 'B03_KP', '2013-06-01 08:00:57', 0),
('KODE8_2013-06-01 03:14:41', 8, 'C15_KP', '2013-06-01 07:58:42', 0),
('KODE8_2013-06-01 03:14:50', 8, 'C15_KP', '2013-06-01 07:58:42', 0),
('KODE11_2013-06-01 03:15:34', 11, 'B03_KP', '2013-06-01 07:15:38', 0),
('KODE8_2013-06-01 03:57:01', 8, 'C15_KP', '2013-06-01 07:58:42', 0),
('KODE8_2013-06-01 03:58:19', 8, 'C15_KP', '2013-06-01 07:58:46', 0),
('KODE10_2013-06-01 03:58:53', 10, 'C06_KN', '2013-06-01 07:58:58', 0),
('KODE8_2013-06-01 04:00:28', 8, 'B02_KP', '2013-06-01 08:00:31', 0),
('KODE11_2013-06-01 04:00:36', 11, 'B03_KP', '2013-06-01 08:00:41', 0),
('KODE18_2013-06-01 04:00:45', 18, 'B03_KP', '2013-06-01 08:01:39', 0),
('KODE10_2013-06-01 04:01:57', 10, 'C02_KN', '2013-06-01 08:02:00', 0),
('KODE10_2013-06-01 05:52:57', 10, 'B09_KN', '2013-06-01 09:53:01', 0),
('KODE24_2013-06-01 07:43:09', 24, 'B31_RS', '2013-06-01 11:43:08', 1),
('KODE21_2013-06-01 07:47:49', 21, 'B31_RS', '2013-06-01 11:47:48', 1),
('KODE20_2013-06-01 07:48:21', 20, 'A31_RS', '2013-06-01 11:49:52', 0),
('KODE29_2013-06-01 07:50:10', 29, 'A31_RS', '2013-06-01 11:50:09', 1),
('KODE18_2013-06-01 07:55:36', 18, 'C01_KP', '2013-06-01 11:55:35', 1),
('KODE31_2013-06-02 11:26:04', 31, 'B32_RS', '2013-06-02 15:27:04', 0),
('KODE31_2013-06-02 11:27:59', 31, 'C01_KP', '2013-06-02 15:27:59', 1),
('KODE27_2013-06-02 11:28:34', 27, 'D01_KP', '2013-06-02 15:28:33', 1),
('KODE30_2013-06-02 11:28:44', 30, 'D01_KP', '2013-06-02 15:28:44', 1),
('KODE25_2013-06-02 11:29:27', 25, 'D01_KP', '2013-06-02 15:29:27', 1),
('KODE16_2013-06-02 11:30:12', 16, 'E01_KP', '2013-06-02 15:30:11', 1),
('KODE28_2013-06-02 11:30:20', 28, 'E01_KP', '2013-06-02 15:30:20', 1),
('KODE23_2013-06-02 11:31:01', 23, 'A31_RS', '2013-06-02 15:31:01', 1),
('KODE15_2013-06-02 11:31:56', 15, 'B10_KN', '2013-06-02 15:32:10', 0),
('KODE15_2013-06-02 12:03:33', 15, 'B10_KN', '2013-06-02 16:03:33', 1),
('KODE32_2013-06-02 12:03:43', 32, 'B10_KN', '2013-06-02 16:03:43', 1),
('KODE26_2013-06-02 12:04:29', 26, 'K01_KN', '2013-06-02 16:04:28', 1),
('KODE35_2013-06-02 12:04:37', 35, 'K01_KN', '2013-06-02 16:04:37', 1),
('KODE33_2013-06-02 12:04:58', 33, 'K26_KN', '2013-06-02 16:04:58', 1),
('KODE36_2013-06-03 16:38:07', 36, 'B01_KP', '2013-06-05 06:49:16', 1),
('KODE11_2013-06-03 22:04:56', 11, 'B42_RS', '2013-06-04 02:04:56', 1),
('KODE20_2013-06-03 22:05:00', 20, 'B42_RS', '2013-06-04 02:05:01', 1),
('KODE8_2013-06-11 17:12:45', 8, 'B02_KP', '2013-06-11 15:12:48', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `asrama`
--

CREATE TABLE IF NOT EXISTS `asrama` (
  `kode` varchar(32) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `jenis_kelamin` tinyint(1) NOT NULL,
  `kontak` varchar(45) NOT NULL,
  `deskripsi` varchar(324) NOT NULL,
  `alamat` varchar(216) NOT NULL,
  `foto1` varchar(45) NOT NULL,
  `foto2` varchar(45) DEFAULT NULL,
  `foto3` varchar(45) DEFAULT NULL,
  `deskripsi_lengkap` varchar(10000) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `asrama`
--

INSERT INTO `asrama` (`kode`, `nama`, `jenis_kelamin`, `kontak`, `deskripsi`, `alamat`, `foto1`, `foto2`, `foto3`, `deskripsi_lengkap`) VALUES
('KN', 'Kanayakan', 0, '085735036969', 'Asrama Kanayakan adalah asrama khusus mahasiswi ITB yang utamanya diperuntukkan bagi mahasiswi penerima beasiswa BIDIK MISI.', 'Jl. Kanayakan Lama No 61, Bandung, Indonesia 40135', '-', NULL, NULL, 'Asrama Kanayakan adalah asrama khusus mahasiswi ITB yang utamanya diperuntukkan bagi mahasiswi penerima beasiswa BIDIK MISI. <br/><br/>Asrama ini terdiri dari 2 gedung yaitu gedung baru (4 lantai) dan gedung lama (3 lantai). Masing-masing  diisi 2-3 Mahasiswi per kamar. Jumlah kamar di gedung lama adalah 34 kamar dan di gedung baru 35 kamar. <br/><br/>Fasilitas yang disediakan adalah meja belajar, kursi dan lemari baju.'),
('KP', 'Kidang Pananjung', 1, '085735036969', 'Asrama Kidang Pananjung adalah asrama khusus mahasiswa ITB yang terletak di daerah Cisitu.', 'Jl. Cisitu Lama VIII Kidang Pananjung Kodepos 40135', 'KP_1.jpg', 'KP_2.JPG', 'KP_3.JPG', 'Asrama Kidang Pananjung adalah asrama khusus mahasiswa ITB. Asrama ini memiliki 6 gedung. Satu gedung utama yaitu gedung A digunakan sebagai kantor dan 5 gedung lainnya (Gedung B,C,D,E,F) adalah gedung asrama.                  <br/><br/>                 Jumlah kamar yang ada di asrama ini adalah 74 kamar dengan luas masing-masing kamar 6 x 6 m2.                                   Fasilitas yang disediakan di dalam kamar terdiri dari 3 tempat tidur, 3 lemari pakaian, 3 meja dan kursi belajar dan 3 rak buku. Fasilitas di luar kamar adalah kamar mandi, parkir mobil dan motor. '),
('RS', 'Rusunawa Sangkuriang', 1, '085735036969', 'Asrama Rusunawa adalah asrama yang diperuntukkan bagi mahasiswa ITB yang terletak di daerah Sangkuriang.  ', 'Jl. Sangkuriang Dalam No. 55, Bandung 40135', 'RS_1.JPG', 'RS_2.JPG', 'RS_3.JPG', 'Asrama Rusunawa adalah asrama yang diperuntukkan bagi mahasiswa ITB yang terletak di daerah Sangkuriang. <br/><br/>Asrama Putera Rusunawa Sangkuriang terdiri dari 2 gedung, yaitu Gedung A dan Gedung B dengan masing-masing gedung mempunyai 48 kamar (5 Lantai). Satu Kamar terdiri dari 2 mahasiswa dengan fasilitas 2 kamar mandi di dalam, 1 dapur, 1 lemari pakaian, 2 meja belajar, 2 kursi belajar, 2 spring bed, jemuran pakaian, dan listrik perkamar daya 450 watt. <br/><br/>Kegiatan-kegiatan yang dilakukan adalah Tutorial, Diskusi dan beberapa event lainnya. ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `beasiswa`
--

CREATE TABLE IF NOT EXISTS `beasiswa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penghuni_id` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_beasiswa_penghuni1` (`penghuni_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `beasiswa`
--

INSERT INTO `beasiswa` (`id`, `penghuni_id`, `nama`, `tahun`) VALUES
(2, 1, 'Memancing', ''),
(3, 1, 'Beasiswa Memancing', '2001'),
(4, 1, '', '1980'),
(5, 1, 'Bidik Misi', '2007');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fakultas`
--

CREATE TABLE IF NOT EXISTS `fakultas` (
  `kode` varchar(45) NOT NULL,
  `nama` varchar(64) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fakultas`
--

INSERT INTO `fakultas` (`kode`, `nama`) VALUES
('STEI', 'Sekolah Tinggi Elektro dan Informatika'),
('FMIPA', 'Fakultas Matematika dan Ilmu Pengetahuan Alam'),
('SITH', 'Sekolah Ilmu dan Teknologi Hayati'),
('SF', 'Sekolah Farmasi'),
('FITB', 'Fakultas Ilmu dan Teknologi Kebumian'),
('FTTM', 'Fakultas Teknik Pertambangan dan Perminyakan'),
('FTI', 'Fakultas Teknologi Industri'),
('FTSL', 'Fakultas Teknik Sipil dan Lingkungan'),
('SAPPK', 'Sekolah Arsitektur, Perencanaan, dan Pengembangan Kebijakan'),
('FSRD', 'Fakultas Seni Rupa dan Desain'),
('SBM', 'Sekolah Bisnis dan Manajemen '),
('FTMD', 'Fakultas Teknik Mesin dan Dirgantara');

-- --------------------------------------------------------

--
-- Struktur dari tabel `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penghuni_id` int(11) NOT NULL,
  `filename` varchar(45) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_file_penghuni1` (`penghuni_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `file`
--

INSERT INTO `file` (`id`, `penghuni_id`, `filename`, `status`) VALUES
(8, 1, 'C:xampp	mpphp8FDD.tmp', 1),
(9, 1, '[Tugas1M-II3042-18209013].docx', 1),
(10, 1, 'bag.jpg', 1),
(11, 1, 'l100_mensslimfit_army.jpg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kamar`
--

CREATE TABLE IF NOT EXISTS `kamar` (
  `nomor_kamar` varchar(11) NOT NULL,
  `asrama_kode` varchar(32) NOT NULL,
  `terisi` int(11) NOT NULL,
  `kapasitas` int(11) NOT NULL,
  `jenis_kelamin` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`nomor_kamar`),
  KEY `fk_kamar_asrama1` (`asrama_kode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kamar`
--

INSERT INTO `kamar` (`nomor_kamar`, `asrama_kode`, `terisi`, `kapasitas`, `jenis_kelamin`, `status`) VALUES
('A01_KN', 'KN', 0, 3, 0, 0),
('A01_RS', 'RS', 0, 2, 1, 0),
('A02_KN', 'KN', 0, 3, 0, 0),
('A02_RS', 'RS', 0, 2, 1, 1),
('A03_KN', 'KN', 0, 3, 0, 0),
('A03_RS', 'RS', 0, 2, 1, 0),
('A04_KN', 'KN', 0, 3, 0, 0),
('A04_RS', 'RS', 0, 2, 1, 0),
('A05_KN', 'KN', 0, 3, 0, 0),
('A05_RS', 'RS', 0, 2, 1, 1),
('A06_KN', 'KN', 0, 3, 0, 0),
('A06_RS', 'RS', 0, 2, 1, 0),
('A07_KN', 'KN', 0, 3, 0, 0),
('A07_RS', 'RS', 0, 2, 1, 0),
('A08_KN', 'KN', 0, 3, 0, 0),
('A08_RS', 'RS', 0, 2, 1, 0),
('A09_KN', 'KN', 0, 3, 0, 0),
('A09_RS', 'RS', 0, 2, 1, 0),
('A10_KN', 'KN', 0, 3, 0, 0),
('A10_RS', 'RS', 0, 2, 1, 0),
('A11_RS', 'RS', 0, 2, 1, 1),
('A12_RS', 'RS', 0, 2, 1, 0),
('A13_RS', 'RS', 0, 2, 1, 1),
('A14_RS', 'RS', 0, 2, 1, 0),
('A15_RS', 'RS', 0, 2, 1, 1),
('A16_RS', 'RS', 0, 2, 1, 1),
('A17_RS', 'RS', 0, 2, 1, 1),
('A18_RS', 'RS', 0, 2, 1, 0),
('A19_RS', 'RS', 0, 2, 1, 1),
('A20_RS', 'RS', 0, 2, 1, 1),
('A21_RS', 'RS', 0, 2, 1, 1),
('A22_RS', 'RS', 0, 2, 1, 1),
('A23_RS', 'RS', 0, 2, 1, 1),
('A24_RS', 'RS', 0, 2, 1, 1),
('A25_RS', 'RS', 0, 2, 1, 1),
('A26_RS', 'RS', 0, 2, 1, 1),
('A27_RS', 'RS', 0, 2, 1, 1),
('A28_RS', 'RS', 0, 2, 1, 1),
('A29_RS', 'RS', 0, 2, 1, 1),
('A30_RS', 'RS', 0, 2, 1, 1),
('A31_RS', 'RS', 0, 2, 1, 1),
('A32_RS', 'RS', 0, 2, 1, 1),
('A33_RS', 'RS', 0, 2, 1, 0),
('A34_RS', 'RS', 0, 2, 1, 1),
('A35_RS', 'RS', 0, 2, 1, 1),
('A36_RS', 'RS', 0, 2, 1, 1),
('B01_KN', 'KN', 0, 3, 0, 1),
('B01_KP', 'KP', 2, 3, 1, 1),
('B01_RS', 'RS', 0, 2, 1, 0),
('B02_KN', 'KN', 0, 3, 0, 1),
('B02_KP', 'KP', -2, 3, 1, 1),
('B02_RS', 'RS', 0, 2, 1, 0),
('B03_KN', 'KN', 0, 3, 0, 1),
('B03_KP', 'KP', 0, 3, 1, 0),
('B03_RS', 'RS', 0, 2, 1, 0),
('B04_KN', 'KN', 0, 3, 0, 1),
('B04_KP', 'KP', 0, 3, 1, 1),
('B04_RS', 'RS', 0, 2, 1, 0),
('B05_KN', 'KN', 0, 3, 0, 1),
('B05_KP', 'KP', 0, 3, 1, 1),
('B05_RS', 'RS', 0, 2, 1, 0),
('B06_KN', 'KN', 0, 3, 0, 1),
('B06_KP', 'KP', 0, 3, 1, 0),
('B06_RS', 'RS', 0, 2, 1, 0),
('B07_KN', 'KN', 0, 3, 0, 1),
('B07_KP', 'KP', 0, 3, 1, 1),
('B07_RS', 'RS', 0, 2, 1, 1),
('B08_KN', 'KN', 0, 3, 0, 1),
('B08_KP', 'KP', 0, 3, 1, 1),
('B08_RS', 'RS', 0, 2, 1, 0),
('B09_KN', 'KN', 0, 3, 0, 1),
('B09_KP', 'KP', 0, 3, 1, 1),
('B09_RS', 'RS', 0, 2, 1, 0),
('B10_KN', 'KN', 0, 3, 0, 1),
('B10_KP', 'KP', 0, 3, 1, 1),
('B10_RS', 'RS', 0, 2, 1, 1),
('B11_KN', 'KN', 0, 3, 0, 1),
('B11_RS', 'RS', 0, 2, 1, 1),
('B12_KN', 'KN', 0, 3, 0, 1),
('B12_RS', 'RS', 0, 2, 1, 1),
('B13_RS', 'RS', 0, 2, 1, 1),
('B14_RS', 'RS', 0, 2, 1, 1),
('B15_RS', 'RS', 0, 2, 1, 0),
('B16_RS', 'RS', 0, 2, 1, 0),
('B17_RS', 'RS', 0, 2, 1, 1),
('B18_RS', 'RS', 0, 2, 1, 1),
('B19_RS', 'RS', 0, 2, 1, 1),
('B20_RS', 'RS', 0, 2, 1, 1),
('B21_RS', 'RS', 0, 2, 1, 1),
('B22_RS', 'RS', 0, 2, 1, 1),
('B23_RS', 'RS', 0, 2, 1, 1),
('B24_RS', 'RS', 0, 2, 1, 1),
('B25_RS', 'RS', 0, 2, 1, 0),
('B26_RS', 'RS', 0, 2, 1, 1),
('B27_RS', 'RS', 0, 2, 1, 1),
('B28_RS', 'RS', 0, 2, 1, 1),
('B29_RS', 'RS', 0, 2, 1, 0),
('B30_RS', 'RS', 0, 2, 1, 1),
('B31_RS', 'RS', 0, 2, 1, 1),
('B32_RS', 'RS', 0, 2, 1, 1),
('B33_RS', 'RS', 0, 2, 1, 1),
('B34_RS', 'RS', 0, 2, 1, 1),
('B35_RS', 'RS', 0, 2, 1, 1),
('B36_RS', 'RS', 0, 2, 1, 1),
('C01_KN', 'KN', 0, 3, 0, 1),
('C01_KP', 'KP', 0, 3, 1, 1),
('C02_KN', 'KN', 0, 3, 0, 1),
('C02_KP', 'KP', 0, 3, 1, 1),
('C03_KN', 'KN', 0, 3, 0, 1),
('C03_KP', 'KP', 0, 3, 1, 1),
('C04_KN', 'KN', 0, 3, 0, 1),
('C04_KP', 'KP', 0, 3, 1, 1),
('C05_KN', 'KN', 0, 3, 0, 1),
('C05_KP', 'KP', 0, 3, 1, 1),
('C06_KN', 'KN', 0, 3, 0, 1),
('C06_KP', 'KP', 0, 3, 1, 1),
('C07_KN', 'KN', 0, 3, 0, 1),
('C07_KP', 'KP', 0, 3, 1, 1),
('C08_KN', 'KN', 0, 3, 0, 1),
('C08_KP', 'KP', 0, 3, 1, 1),
('C09_KN', 'KN', 0, 3, 0, 1),
('C09_KP', 'KP', 0, 3, 1, 1),
('C10_KN', 'KN', 0, 3, 0, 1),
('C10_KP', 'KP', 0, 3, 1, 1),
('C11_KN', 'KN', 0, 3, 0, 1),
('C11_KP', 'KP', 0, 3, 1, 1),
('C12_KN', 'KN', 0, 3, 0, 1),
('C12_KP', 'KP', 0, 3, 1, 1),
('C13_KP', 'KP', 0, 3, 1, 1),
('C14_KP', 'KP', 0, 3, 1, 1),
('C15_KP', 'KP', 0, 3, 1, 1),
('C16_KP', 'KP', 0, 3, 1, 1),
('C17_KP', 'KP', 0, 3, 1, 1),
('C18_KP', 'KP', 0, 3, 1, 1),
('D01_KP', 'KP', 0, 3, 1, 1),
('D02_KP', 'KP', 0, 3, 1, 1),
('D03_KP', 'KP', 0, 3, 1, 1),
('D04_KP', 'KP', 0, 3, 1, 1),
('D05_KP', 'KP', 0, 3, 1, 1),
('D06_KP', 'KP', 0, 3, 1, 1),
('D07_KP', 'KP', 0, 3, 1, 1),
('D08_KP', 'KP', 0, 3, 1, 1),
('D09_KP', 'KP', 0, 3, 1, 1),
('D10_KP', 'KP', 0, 3, 1, 1),
('D11_KP', 'KP', 0, 3, 1, 1),
('D12_KP', 'KP', 0, 3, 1, 1),
('D13_KP', 'KP', 0, 3, 1, 1),
('D14_KP', 'KP', 0, 3, 1, 1),
('D15_KP', 'KP', 0, 3, 1, 1),
('D16_KP', 'KP', 0, 3, 1, 1),
('D17_KP', 'KP', 0, 3, 1, 1),
('D18_KP', 'KP', 0, 3, 1, 1),
('D19_KP', 'KP', 0, 3, 1, 1),
('D20_KP', 'KP', 0, 3, 1, 1),
('E01_KP', 'KP', 0, 3, 1, 1),
('E02_KP', 'KP', 0, 3, 1, 1),
('E03_KP', 'KP', 0, 3, 1, 1),
('E04_KP', 'KP', 0, 3, 1, 1),
('E05_KP', 'KP', 0, 3, 1, 1),
('E06_KP', 'KP', 0, 3, 1, 1),
('E07_KP', 'KP', 0, 3, 1, 1),
('E08_KP', 'KP', 0, 3, 1, 1),
('E09_KP', 'KP', 0, 3, 1, 1),
('E10_KP', 'KP', 0, 3, 1, 1),
('E11_KP', 'KP', 0, 3, 1, 1),
('E12_KP', 'KP', 0, 3, 1, 1),
('E13_KP', 'KP', 0, 3, 1, 1),
('E14_KP', 'KP', 0, 3, 1, 1),
('E15_KP', 'KP', 0, 3, 1, 1),
('E16_KP', 'KP', 0, 3, 1, 1),
('F01_KP', 'KP', 0, 3, 1, 0),
('F02_KP', 'KP', 0, 3, 1, 0),
('F03_KP', 'KP', 0, 3, 1, 0),
('F04_KP', 'KP', 0, 3, 1, 0),
('F05_KP', 'KP', 0, 3, 1, 0),
('F06_KP', 'KP', 0, 3, 1, 0),
('F07_KP', 'KP', 0, 3, 1, 0),
('F08_KP', 'KP', 0, 3, 1, 0),
('F09_KP', 'KP', 0, 3, 1, 0),
('F10_KP', 'KP', 0, 3, 1, 0),
('K01_KN', 'KN', 0, 3, 0, 1),
('K02_KN', 'KN', 0, 3, 0, 1),
('K03_KN', 'KN', 0, 3, 0, 1),
('K04_KN', 'KN', 0, 3, 0, 1),
('K05_KN', 'KN', 0, 3, 0, 1),
('K06_KN', 'KN', 0, 3, 0, 1),
('K07_KN', 'KN', 0, 3, 0, 1),
('K08_KN', 'KN', 0, 3, 0, 1),
('K09_KN', 'KN', 0, 3, 0, 1),
('K10_KN', 'KN', 0, 3, 0, 1),
('K11_KN', 'KN', 0, 3, 0, 1),
('K12_KN', 'KN', 0, 3, 0, 1),
('K13_KN', 'KN', 0, 3, 0, 1),
('K14_KN', 'KN', 0, 3, 0, 1),
('K15_KN', 'KN', 0, 3, 0, 1),
('K16_KN', 'KN', 0, 3, 0, 1),
('K17_KN', 'KN', 0, 3, 0, 1),
('K18_KN', 'KN', 0, 3, 0, 1),
('K19_KN', 'KN', 0, 3, 0, 1),
('K20_KN', 'KN', 0, 3, 0, 1),
('K21_KN', 'KN', 0, 3, 0, 1),
('K22_KN', 'KN', 0, 3, 0, 1),
('K23_KN', 'KN', 0, 3, 0, 1),
('K24_KN', 'KN', 0, 3, 0, 1),
('K25_KN', 'KN', 0, 3, 0, 1),
('K26_KN', 'KN', 0, 3, 0, 1),
('K27_KN', 'KN', 0, 3, 0, 1),
('K28_KN', 'KN', 0, 3, 0, 1),
('K29_KN', 'KN', 0, 3, 0, 1),
('K30_KN', 'KN', 0, 3, 0, 1),
('K31_KN', 'KN', 0, 3, 0, 1),
('K32_KN', 'KN', 0, 3, 0, 0),
('K33_KN', 'KN', 0, 3, 0, 1),
('K34_KN', 'KN', 0, 3, 0, 1),
('K35_KN', 'KN', 0, 3, 0, 1),
('B37_RS', 'RS', 0, 2, 1, 1),
('A37_RS', 'RS', 0, 2, 1, 1),
('A38_RS', 'RS', 0, 2, 1, 1),
('A39_RS', 'RS', 0, 2, 1, 1),
('A40_RS', 'RS', 0, 2, 1, 1),
('A41_RS', 'RS', 0, 2, 1, 0),
('A42_RS', 'RS', 0, 2, 1, 0),
('A43_RS', 'RS', 0, 2, 1, 0),
('A44_RS', 'RS', 0, 2, 1, 0),
('A45_RS', 'RS', 0, 2, 1, 1),
('A46_RS', 'RS', 0, 2, 1, 1),
('A47_RS', 'RS', 0, 2, 1, 0),
('A48_RS', 'RS', 0, 2, 1, 1),
('B38_RS', 'RS', 0, 2, 1, 1),
('B39_RS', 'RS', 0, 2, 1, 1),
('B40_RS', 'RS', 0, 2, 1, 1),
('B41_RS', 'RS', 0, 2, 1, 1),
('B42_RS', 'RS', 2, 2, 1, 1),
('B43_RS', 'RS', 0, 2, 1, 0),
('B44_RS', 'RS', 0, 2, 1, 1),
('B45_RS', 'RS', 0, 2, 1, 1),
('B46_RS', 'RS', 0, 2, 1, 1),
('B47_RS', 'RS', 0, 2, 1, 1),
('B48_RS', 'RS', 0, 2, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecakapan`
--

CREATE TABLE IF NOT EXISTS `kecakapan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penghuni_id` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `kemahiran` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_kecakapan_penghuni1` (`penghuni_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `kecakapan`
--

INSERT INTO `kecakapan` (`id`, `penghuni_id`, `nama`, `kemahiran`) VALUES
(1, 1, 'Memancing', 3),
(2, 1, '', 1),
(3, 1, 'Mengadu Domba', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan`
--

CREATE TABLE IF NOT EXISTS `kegiatan` (
  `id_kegiatan` varchar(16) NOT NULL,
  `nama_kegiatan` varchar(64) NOT NULL,
  `tgl_pelaksanaan` date NOT NULL,
  `tempat_pelaksanaan` varchar(64) NOT NULL,
  `deskripsi_kegiatan` varchar(256) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kegiatan`),
  UNIQUE KEY `id_kegiatan` (`id_kegiatan`),
  KEY `nama_kegiatan` (`nama_kegiatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kegiatan`
--

INSERT INTO `kegiatan` (`id_kegiatan`, `nama_kegiatan`, `tgl_pelaksanaan`, `tempat_pelaksanaan`, `deskripsi_kegiatan`, `last_update`) VALUES
('1130515001', 'Latihan Kepemimpinan', '2014-04-23', 'Asrama Kidang Pananjung', 'Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.', '2014-03-31 22:27:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `nim_mahasiswa` varchar(8) NOT NULL,
  `nama_mahasiswa` varchar(64) NOT NULL,
  `letak_asrama` varchar(32) NOT NULL,
  `no_kamar` int(11) NOT NULL,
  PRIMARY KEY (`nim_mahasiswa`),
  UNIQUE KEY `nim_mahasiswa` (`nim_mahasiswa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim_mahasiswa`, `nama_mahasiswa`, `letak_asrama`, `no_kamar`) VALUES
('16511004', 'Thea Olivia', 'Asrama Kanayakan', 1),
('16511013', 'Muhammad Furqan Habibi', 'Asrama Jatinganor', 2),
('16511014', 'David Setyanugraha', 'Asrama Jatinganor', 3),
('16511015', 'Lukman Hakim', 'Asrama Sangkuriang', 4),
('16511018', 'Ridho Akbarisanto', 'Asrama Sangkuriang', 5),
('16511024', 'Krisna Fathurahman', 'Asrama Kidang Pananjung', 6),
('16511027', 'Salvian Reynaldi', 'Asrama Kidang Pananjung', 7),
('16511032', 'Muhammad Rian Fakhrusy', 'Asrama Sangkuriang', 8),
('16511034', 'Kelvin Valensius', 'Asrama Jatinganor', 9),
('16511035', 'Mohammad Agung Prasetyo', 'Asrama Jatinganor', 1),
('16511036', 'Bintang Rahmatullah', 'Asrama Kidang Pananjung', 2),
('16511037', 'Dyah Rahmawati', 'Asrama Kanayakan', 3),
('16511041', 'Alif Raditya Rochman', 'Asrama Kidang Pananjung', 4),
('16511046', 'Riandy Rahman Nugraha', 'Asrama Jatinganor', 5),
('16511052', 'Akwila Elmer B B', 'Asrama Jatinganor', 6),
('16511059', 'Fransiskus Xaverius Christian', 'Asrama Sangkuriang', 7),
('16511062', 'Rangga Yustian M', 'Asrama Sangkuriang', 8),
('16511068', 'Tito D Kesumo Siregar', 'Asrama Sangkuriang', 9),
('16511069', 'Ignatius Evan Daryanto', 'Asrama Kidang Pananjung', 1),
('16511080', 'Arief Rahman', 'Asrama Kidang Pananjung', 2),
('16511085', 'Baharudin Afif Suryanugraha', 'Asrama Kidang Pananjung', 3),
('16511088', 'Ongki Herlambang', 'Asrama Sangkuriang', 4),
('16511089', 'Mikael Tamirin', 'Asrama Kidang Pananjung', 5),
('16511094', 'Pratama Havidiarahman Suhono', 'Asrama Kidang Pananjung', 6),
('16511101', 'Lubis Sucipto', 'Asrama Sangkuriang', 7),
('16511104', 'Akbar Juang Saputra', 'Asrama Kidang Pananjung', 8),
('16511107', 'Fathan Adi Pranaya', 'Asrama Kidang Pananjung', 9),
('16511111', 'Azalea Fisitania', 'Asrama Kanayakan', 10),
('16511118', 'Sonny Lazuardi Hermawan', 'Asrama Sangkuriang', 11),
('16511120', 'Mochammad Dikra Prasetya', 'Asrama Sangkuriang', 12),
('16511122', 'Aldi Doanta Kurnia', 'Asrama Kidang Pananjung', 13),
('16511135', 'Adhitya Ramadhanus', 'Asrama Sangkuriang', 14),
('16511136', 'Isabella Julia Putri', 'Asrama Kanayakan', 15),
('16511139', 'Greha Devana Candra', 'Asrama Kidang Pananjung', 16),
('16511143', 'Aldy Wirawan', 'Asrama Sangkuriang', 17),
('16511149', 'Jais Anasrulloh Jafari', 'Asrama Sangkuriang', 18),
('16511152', 'Muhammad Rizky W', 'Asrama Kidang Pananjung', 19),
('16511157', 'Riefky Amarullah Romadhoni', 'Asrama Sangkuriang', 10),
('16511158', 'Yanuar Aristya Edy Putra', 'Asrama Kidang Pananjung', 11),
('16511161', 'Wishnu', 'Asrama Kidang Pananjung', 12),
('16511163', 'Evan Budianto', 'Asrama Sangkuriang', 13),
('16511172', 'Bima Laksmana Pramudita', 'Asrama Kidang Pananjung', 14),
('16511175', 'Mohamad Rivai Ramandhani', 'Asrama Kidang Pananjung', 15),
('16511183', 'Muhammad Nassirudin', 'Asrama Kidang Pananjung', 16),
('16511189', 'Reno Rasyad', 'Asrama Jatinganor', 17),
('16511190', 'M Harits Shalahuddin Adil H E', 'Asrama Jatinganor', 18),
('16511193', 'Fadli Demitra', 'Asrama Kidang Pananjung', 19),
('16511202', 'Akbar Suryowibowo Syam', 'Asrama Kidang Pananjung', 10),
('16511203', 'Muhamad Ihsan', 'Asrama Kidang Pananjung', 11),
('16511213', 'Mohamad Ramdan Fadilah', 'Asrama Sangkuriang', 12),
('16511216', 'Andreas Dwi Nugroho', 'Asrama Sangkuriang', 13),
('16511222', 'Ananda Kurniawan P', 'Asrama Sangkuriang', 14),
('16511223', 'Michael Ingga Gunawan', 'Asrama Kidang Pananjung', 15),
('16511224', 'Innani Yudhoafi', 'Asrama Kanayakan', 16),
('16511226', 'Harfin Yusuf Biu', 'Asrama Kidang Pananjung', 17),
('16511231', 'Sabituddin', 'Asrama Kidang Pananjung', 18),
('16511234', 'Destra Bintang Perkasa', 'Asrama Kidang Pananjung', 19),
('16511235', 'Albhikautsar Dharma Kesuma', 'Asrama Kidang Pananjung', 10),
('16511236', 'Yogi Salomo Mangontang Pratama', 'Asrama Sangkuriang', 11),
('16511239', 'Muhammad Zen', 'Asrama Sangkuriang', 12),
('16511241', 'Adhika Aryantio', 'Asrama Jatinganor', 13),
('16511246', 'Arief Pradana', 'Asrama Jatinganor', 14),
('16511247', 'Renusa Andra Prayogo', 'Asrama Jatinganor', 15),
('16511251', 'Muhammad Ikhsan', 'Asrama Kidang Pananjung', 16),
('16511275', 'Erwin', 'Asrama Kidang Pananjung', 17),
('16511276', 'Rifki Afina Putri', 'Asrama Kanayakan', 18),
('16511279', 'Rama Febriyan', 'Asrama Kidang Pananjung', 19),
('16511281', 'Daniel', 'Asrama Kidang Pananjung', 20),
('16511283', 'Habibie Faried', 'Asrama Sangkuriang', 21),
('16511287', 'Ryan Ignatius Hadiwijaya', 'Asrama Sangkuriang', 22),
('16511301', 'Setyo Legowo', 'Asrama Sangkuriang', 23),
('16511302', 'Willy Gotama', 'Asrama Kidang Pananjung', 24),
('16511303', 'Iskandar Setiadi', 'Asrama Kidang Pananjung', 25),
('16511304', 'Alifa Nurani Putri', 'Asrama Kanayakan', 20),
('16511306', 'Ichlasul Amal', 'Asrama Sangkuriang', 21),
('16511310', 'Raden Fajar Hadria Putra', 'Asrama Sangkuriang', 22),
('16511312', 'Mahessa Ramadhana', 'Asrama Jatinganor', 23),
('16511321', 'Taufik Hidayat', 'Asrama Jatinganor', 24),
('16511322', 'Kevin Verdi', 'Asrama Jatinganor', 25),
('16511323', 'Faiz Ilham Muhammad', 'Asrama Kidang Pananjung', 20),
('16511327', 'Farizan Ramadhan', 'Asrama Kidang Pananjung', 21),
('16511330', 'Arini Hasianna', 'Asrama Kanayakan', 22),
('16511348', 'Fawwaz Muhammad', 'Asrama Kidang Pananjung', 23),
('16511361', 'Rifkiansyah Meidian', 'Asrama Kidang Pananjung', 24),
('16511364', 'Nurwanto', 'Asrama Kidang Pananjung', 25),
('16511369', 'Willy Fitra Hendria', 'Asrama Kidang Pananjung', 20),
('16511372', 'Dinah Kamilah Ulfa', 'Asrama Kanayakan', 21),
('16511376', 'Muhamad Andri Eka Fauzy', 'Asrama Sangkuriang', 22),
('16511378', 'James Jaya', 'Asrama Sangkuriang', 23),
('16511383', 'Pandu Kartika Putra', 'Asrama Jatinganor', 24);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mulai_bayar`
--

CREATE TABLE IF NOT EXISTS `mulai_bayar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(50) NOT NULL,
  `tanggal_mulai_bayar` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `mulai_bayar`
--

INSERT INTO `mulai_bayar` (`id`, `id_user`, `tanggal_mulai_bayar`) VALUES
(1, '10110083', '2014-04-30'),
(2, '0', '2014-04-26'),
(3, '10710084', '2014-04-18'),
(4, '111', '2014-04-29'),
(5, '13511046', '2014-02-01'),
(6, '1111111111', '2014-04-25'),
(7, '1123123', '2014-04-30'),
(8, '12011001', '2014-05-30'),
(9, '1231', '2014-06-11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE IF NOT EXISTS `pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `nominal` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `bulan_pembayaran` date NOT NULL,
  `bukti` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Belum',
  `keterangan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `user`, `nominal`, `tanggal`, `bulan_pembayaran`, `bukti`, `status`, `keterangan`) VALUES
(32, '13511046', 100000, '2014-04-27', '2014-02-01', '535cf0bb650b3legend.jpg', 'Sudah', 'OK'),
(33, '13511046', 150000, '2014-04-29', '2014-03-01', '535f13846003flogo OHL.jpg', 'Sudah', 'OK'),
(34, '13511046', 100000, '2014-04-29', '2014-04-01', '535f13f394d9clegend.jpg', 'Sudah', 'OK'),
(36, '13511046', 150000, '2014-04-29', '2014-04-01', '535f19b1bfc72itb.PNG', 'Belum', 'N/A'),
(38, '13511046', 150000, '2014-05-22', '2014-05-01', '537d92513ba4310264607_10201999548526306_571208380_n.jpg', 'Sudah', 'OK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendidikan`
--

CREATE TABLE IF NOT EXISTS `pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penghuni_id` int(11) NOT NULL,
  `nama_sekolah` varchar(45) NOT NULL,
  `tahun_lulus` varchar(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pendidikan_penghuni1` (`penghuni_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `pendidikan`
--

INSERT INTO `pendidikan` (`id`, `penghuni_id`, `nama_sekolah`, `tahun_lulus`) VALUES
(1, 1, 'SDN 001', '2000'),
(2, 1, 'Institut Teknologi Bandung', '2013');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penghuni`
--

CREATE TABLE IF NOT EXISTS `penghuni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_NID` varchar(32) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `NIM` varchar(32) NOT NULL,
  `nama_panggilan` varchar(45) DEFAULT NULL,
  `agama` varchar(45) DEFAULT NULL,
  `suku_bangsa` varchar(45) DEFAULT NULL,
  `gol_darah` varchar(45) DEFAULT NULL,
  `nama_ayah` varchar(45) DEFAULT NULL,
  `nama_ibu` varchar(45) DEFAULT NULL,
  `jumlah_saudara` int(11) DEFAULT NULL,
  `asal_sma` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_penghuni_user` (`user_NID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data untuk tabel `penghuni`
--

INSERT INTO `penghuni` (`id`, `user_NID`, `status`, `NIM`, `nama_panggilan`, `agama`, `suku_bangsa`, `gol_darah`, `nama_ayah`, `nama_ibu`, `jumlah_saudara`, `asal_sma`) VALUES
(9, '18209013(2)', 0, 'NIM BELUM ADA', 'Gharta', 'Islam', 'Batak', 'A', 'G', 'H', 2, ''),
(2, '18209019', 0, '', 'wan', 'islam', 'sunda', 'AB', 'em', 'reed', 2, ''),
(3, '111', 0, '', 'wan', 'islam', 'sunda', 'A', 'em', 'reed', 2, ''),
(4, '18209014', 0, '', 'Gharta', 'Islam', 'Jawa', 'O', 'AA', 'BB', 1, ''),
(5, '182090133', 0, 'NIM BELUM ADA', 'gh', 'islam', 'batak', 'O', 'gu', 'har', 2, ''),
(6, '1123123', 0, 'NIM BELUM ADA', 'asdasd', 'asdasd', 'asdasd', 'AB', 'asdasd', 'asdad', 10, ''),
(7, '18209005', 0, 'NIM BELUM ADA', 'Nenek', 'Islam', 'Rusia', 'O', 'Ikbal', 'Iskandar', 1, ''),
(8, '18209013', 0, 'NIM BELUM ADA', 'Gharta', 'Islam', 'Batak', 'O', 'Guntur', 'Haryani', 2, ''),
(10, '2345456', 0, 'NIM BELUM ADA', 'Tami', 'Islam', 'di isi Indonesia atau Jawa ya?', 'O', 'Bapakku', 'Ibuku', 4, ''),
(11, '13711052', 1, 'NIM BELUM ADA', 'Angga', 'Islam', 'Jawa', 'A', 'Wadi', 'Hadijah', 1, ''),
(12, '16512115', 1, 'NIM BELUM ADA', 'Eki atau Ajo', 'Islam', 'Sikumbang-Minangkabau', 'AB', 'Djufri Ade', 'Elida', 2, ''),
(13, '13510005', 1, 'NIM BELUM ADA', 'Adzul', 'Islam', 'Minang, Sunda', 'A', 'Muhammad Syahrul', 'Rohanah Setiawati', 3, ''),
(14, '16012176', 1, 'NIM BELUM ADA', 'fani', 'islam', 'sunda', 'O', 'endang amzah somantri', 'nurhastati', 2, ''),
(15, '17510024', 1, 'NIM BELUM ADA', 'Elin', 'Islam', 'Sunda', 'O', 'Hendi Junaedi', 'Yenti Nuryamah', 6, ''),
(16, '10710084', 1, 'NIM BELUM ADA', 'Untung ', 'Kristen Protestan', 'Indonesia', 'O', 'Sadrach', 'Ester Yo', 1, ''),
(17, '13110046', 1, 'NIM BELUM ADA', 'Heri', 'Islam', 'Indonesia', 'B', 'Ahmad Santoso', 'Tukirah', 1, ''),
(18, '16812149', 1, 'NIM BELUM ADA', 'Angga', 'Islam', 'Jawa', 'O', 'Siswanto', 'Wiwik Surawi', 1, ''),
(19, '15010107', 1, 'NIM BELUM ADA', 'Sri', 'Islam', 'Minangkabau', 'B', 'Syafril', 'Mulyani', 6, ''),
(20, '16712224', 1, 'NIM BELUM ADA', 'pilemon', 'kristen protestan', 'simalungun', 'B', 'Marjan sinaga', 'Soniwaty girsang', 3, ''),
(26, '13310105', 1, 'NIM BELUM ADA', 'Shani', 'Islam', 'Banten', 'A', 'Taufiq', 'Umdah', 3, ''),
(21, '13110102', 1, 'NIM BELUM ADA', 'nanda', 'islam', 'jawa', 'O', 'Daryono', 'Sri Kandaryati', 1, ''),
(22, '16112078', 1, 'NIM BELUM ADA', 'Fauzi', 'Islam', 'Nasution-Batak', 'A', 'Buyung Nasution', 'Misiyem', 2, ''),
(23, '10110083', 1, 'NIM BELUM ADA', 'ipin', 'Islam', 'Jawa', 'AB', 'Masngut', 'Kasri', 2, ''),
(24, '16412280', 1, 'NIM BELUM ADA', 'Ipul', 'Islam', 'Sunda', 'AB', 'Dede suherlan', 'Hodijah', 3, ''),
(25, '19012169', 1, 'NIM BELUM ADA', 'Dani', 'Islam', 'Jawa', 'B', 'Nur Ikhsan', 'Musrifah', 2, ''),
(27, '12011001', 1, 'NIM BELUM ADA', 'Wisnu', 'Islam', 'Jawa', 'B', 'Iman Sukirman', 'Suhitit', 2, ''),
(28, '12911018', 1, 'NIM BELUM ADA', 'Riam', 'Islam', 'Sunda', 'B', 'Tedi Sunandar', 'Sutad Padmi Yuswantini', 2, ''),
(29, '15111052', 1, 'NIM BELUM ADA', 'Rio', 'Islam', 'Sunda', 'AB', 'Adang', 'Rida Tita', 3, ''),
(30, '15111082', 1, 'NIM BELUM ADA', 'Ulin', 'Islam', 'Jawa', 'O', 'Achmad Mirza', 'Himmah Taulany', 2, ''),
(31, '13610023', 1, 'NIM BELUM ADA', 'Arri', 'Islam', 'Jawa', 'O', 'Rakiantono', 'Rumini', 1, ''),
(32, '16112076', 1, 'NIM BELUM ADA', 'rika', 'islam', 'indonesia', 'AB', 'yayat ruhiyat', 'apong nuraeni', 1, ''),
(33, '16112054', 1, 'NIM BELUM ADA', 'Inab, Za', 'Islam', 'Sunda', 'O', 'Nasrul Jatnika', 'Irma R Mandaka', 5, ''),
(34, '0', 0, 'NIM BELUM ADA', 'Agung', 'Islam', 'Jawa', 'O', 'Rahasia', 'Untuk ID lain', 7, ''),
(35, '13011070', 1, 'NIM BELUM ADA', 'annur', 'islam', 'jawa', 'B', 'Muhammad Ngisom', 'Sunnatul Awalin', 4, ''),
(36, '16612306', 1, 'NIM BELUM ADA', 'Andika', 'Islam', 'Indonesia', 'A', 'Suparto', 'Emy', 2, ''),
(37, '1111111111', 0, 'NIM BELUM ADA', 'sdasas', 'sacwqrf43', 'r43 wr4', 'A', 'd 2c2 2rc2r2c4', '-', 0, 'asdasda');

-- --------------------------------------------------------

--
-- Struktur dari tabel `periode`
--

CREATE TABLE IF NOT EXISTS `periode` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `periode`
--

INSERT INTO `periode` (`id`, `nama`, `tanggal_mulai`, `tanggal_akhir`) VALUES
(0, 'Tahun Ajaran 2011/2012', '2014-04-01', '2014-04-30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `program_studi`
--

CREATE TABLE IF NOT EXISTS `program_studi` (
  `kode` varchar(32) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `fakultas_kode` varchar(45) NOT NULL,
  PRIMARY KEY (`kode`),
  KEY `fk_program_studi_fakultas1` (`fakultas_kode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `semester`
--

CREATE TABLE IF NOT EXISTS `semester` (
  `id` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `tanggal_mulai` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
  `kode` varchar(20) NOT NULL,
  `user_NID` varchar(32) NOT NULL,
  `is_available` tinyint(1) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`kode`),
  KEY `fk_ticket_user1` (`user_NID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ticket`
--

INSERT INTO `ticket` (`kode`, `user_NID`, `is_available`, `waktu`) VALUES
('3cb9d01fb2c43d46a', '18209005', 1, '2013-05-14 14:48:27'),
('4bba44280', '1231', 1, '2013-04-11 17:31:22'),
('694a3030e9999ddd0c5e', '111', 1, '2013-04-11 17:29:26'),
('846134d83481a92aae8c', '18209013', 1, '2013-04-06 18:52:24'),
('87c39ce5ef2f971ef3a9', '182090133', 1, '2013-04-23 14:59:04'),
('a1d32cf9766deaca359b', 'adminasrama', 1, '2013-04-23 11:41:25'),
('a7021f0cf0fd0', '181818181', 1, '2013-04-11 17:28:30'),
('a71bd62c665', '18209019', 1, '2013-04-21 15:04:15'),
('d63b4987d3a74017f74e', '18209014', 1, '2013-04-22 21:00:45'),
('f315cd939', '1123123', 1, '2013-04-23 15:00:46'),
('98e6a6263666d3bd84', '18209013', 1, '2013-05-30 14:35:59'),
('730692ff2d725dfaa731', '09012728', 1, '2013-05-30 14:45:53'),
('90ce3ad13fce6a064536', '18209013(2)', 1, '2013-05-30 14:51:12'),
('f51f2e719', '123456', 1, '2013-05-30 15:08:11'),
('7e6c00611fa9921514a8', '2345456', 1, '2013-05-30 15:21:17'),
('d341714', '13711052', 1, '2013-05-31 02:53:36'),
('e010dd113efdeeca4980', '16512115', 1, '2013-05-31 02:55:24'),
('033d77ea7dcac92d2', '13510005', 1, '2013-05-31 03:22:50'),
('bcb1eac9ba5a01a46288', '16012176', 1, '2013-05-31 03:41:12'),
('715e6ef939ded29', '17510024', 1, '2013-05-31 03:52:02'),
('aeb6cbdac2d', '10710084', 1, '2013-05-31 04:26:48'),
('35c9bb', '13110046', 1, '2013-05-31 04:52:19'),
('8579b3803c0', '16812149', 1, '2013-05-31 04:59:09'),
('d8040071af3fe916f654', '15010107', 1, '2013-05-31 05:11:20'),
('a838d9b29244', '16712224', 1, '2013-05-31 05:45:12'),
('d03fdbd4c27ebb85ef7c', '13110102', 1, '2013-05-31 06:32:39'),
('49a056d', '16112078', 1, '2013-05-31 06:46:37'),
('98aff9', '10110083', 1, '2013-05-31 06:56:37'),
('afde9f5f1a589ec14e85', '16412280', 1, '2013-05-31 07:18:26'),
('f17487b601f', '19012169', 1, '2013-05-31 07:21:19'),
('2b561e9e0', '13310105', 1, '2013-05-31 10:37:52'),
('7cbb5d88a389c75e5cfa', '12011001', 1, '2013-05-31 17:08:08'),
('1f5454d7d03f4a5bc17a', '12911018', 1, '2013-06-01 01:57:48'),
('bb218ad92bb3ffd7a645', '15111052', 1, '2013-06-01 04:05:43'),
('6dc6e6c90d5d', '15111082', 1, '2013-06-01 04:16:38'),
('365a7fb', '13610023', 1, '2013-06-01 04:34:19'),
('48be050d54bf0', '16112076', 1, '2013-06-01 11:46:50'),
('633bbd77', '16112054', 1, '2013-06-01 23:15:15'),
('bde12cacec13aac5ce9b', '0', 1, '2013-06-02 00:35:22'),
('b9d3de6cc7b', '13011070', 1, '2013-06-02 03:48:59'),
('9d8c2ce1eda79eac6f', '16612306', 1, '2013-06-03 03:50:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tugas`
--

CREATE TABLE IF NOT EXISTS `tugas` (
  `nim_mahasiswa` varchar(8) NOT NULL,
  `nama_mahasiswa` varchar(50) NOT NULL,
  `status` varchar(5) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  PRIMARY KEY (`nim_mahasiswa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tugas`
--

INSERT INTO `tugas` (`nim_mahasiswa`, `nama_mahasiswa`, `status`, `file_name`) VALUES
('16511161', 'Wishnu', 'Sudah', 'C:\\text.txt');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tutor`
--

CREATE TABLE IF NOT EXISTS `tutor` (
  `nim_tutor` varchar(8) NOT NULL,
  `nama_tutor` varchar(64) NOT NULL,
  `letak_asrama` varchar(32) NOT NULL,
  PRIMARY KEY (`nim_tutor`),
  UNIQUE KEY `nim_tutor` (`nim_tutor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tutor`
--

INSERT INTO `tutor` (`nim_tutor`, `nama_tutor`, `letak_asrama`) VALUES
('13510004', 'Ahmad Fauzan', 'Asrama Sangkuriang'),
('13510005', 'Aisyah Dzulqaidah', 'Asrama Kanayakan'),
('13510006', 'David Eko Wibowo', 'Asrama Jatinganor'),
('13510007', 'Tubagus Andhika Nugraha', 'Asrama Sangkuriang'),
('13510008', 'Mohammad Anugrah Sulaeman', 'Asrama Sangkuriang'),
('13510009', 'Sonny Fitra Arfian', 'Asrama Kidang Pananjung'),
('13510010', 'Aditya Agung Putra', 'Asrama Kidang Pananjung'),
('13510011', 'Danny Andrianto', 'Asrama Sangkuriang'),
('13510012', 'Nurul Fithria Lubis', 'Asrama Kanayakan'),
('13510013', 'Patrick Lumban Tobing', 'Asrama Jatinganor'),
('13510014', 'R. Purwoko Cahyo Nugroho', 'Asrama Kidang Pananjung'),
('13510015', 'Wilson Fonda', 'Asrama Sangkuriang'),
('13510016', 'Irvan Aditya', 'Asrama Kidang Pananjung'),
('13510017', 'Vincentius Martin', 'Asrama Jatinganor'),
('13510018', 'Arie Tando', 'Asrama Jatinganor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `NID` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `jenis_kelamin` tinyint(1) NOT NULL,
  `nomor_kontak` varchar(32) NOT NULL,
  `tempat_tanggal_lahir` varchar(216) NOT NULL,
  `fakultas_kode` varchar(45) NOT NULL,
  `status` int(11) NOT NULL,
  `golongan_darah` varchar(4) NOT NULL,
  `alamat` varchar(512) NOT NULL,
  `penyakit` varchar(512) NOT NULL,
  `status_tinggal` varchar(32) NOT NULL,
  `alamat_tinggal` varchar(512) NOT NULL,
  PRIMARY KEY (`NID`),
  KEY `fk_user_fakultas1` (`fakultas_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`NID`, `email`, `password`, `nama`, `jenis_kelamin`, `nomor_kontak`, `tempat_tanggal_lahir`, `fakultas_kode`, `status`, `golongan_darah`, `alamat`, `penyakit`, `status_tinggal`, `alamat_tinggal`) VALUES
('0', 'admin@asrama', '21232f297a57a5a743894a0e4a801fc3', 'Agung Wiyono', 1, '085624449859', 'Sukoharjo, 2 Juni 1959', 'FTSL', 1, '', '', '', '', ''),
('10110083', 'yulianto.arifin17@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Yulianto N Arifin', 1, '085793157828', 'Blitar, 1 Juli 1992', 'FMIPA', 1, '', '', '', '', ''),
('10710084', 'gunawan091191@hotmail.com ', '21232f297a57a5a743894a0e4a801fc3', 'Untung Gunawan ', 1, '08986038068', 'Bandung, 9 November 1992 ', 'SF', 1, '', '', '', '', ''),
('111', 'fefe', '21232f297a57a5a743894a0e4a801fc3', 'fefe', 1, '0181818', '81181881', 'STEI', 0, '', '', '', '', ''),
('1111111111', 'tesaja', '21232f297a57a5a743894a0e4a801fc3', 'tesaja', 0, 'tes', 'tesasdasd', 'FMIPA', 1, '', '', '', '', ''),
('1123123', 'asdasd', '21232f297a57a5a743894a0e4a801fc3', 'asdasd', 1, 'asdasda', 'asdasda', 'STEI', 0, '', '', '', '', ''),
('12011001', 'wisnuanugrahpratama@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Wisnu Anugrah Pratama', 1, '085320739351', 'Indramayu, 16 Mei 1993', 'FITB', 1, '', '', '', '', ''),
('1231', 'fefe2', '21232f297a57a5a743894a0e4a801fc3', 'fefe', 1, '0181818', '81181881', 'STEI', 0, '', '', '', '', ''),
('12911018', 'riambadriana@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Mochamad Riam Badriana', 1, '08986034309', 'Bandung', 'FITB', 1, '', '', '', '', ''),
('13011070', 'annurdyah.fitriana@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'annur dyah fitriana', 0, '085703065590', 'karanganyar, 25 Maret 1993', 'FTI', 1, '', '', '', '', ''),
('13110046', 'heriawan64@ymail.com', '21232f297a57a5a743894a0e4a801fc3', 'Heriawan', 1, '08562384893', 'Banyumas 4 Juli 1992', 'FTMD', 1, '', '', '', '', ''),
('13110102', 'nandavit.setiawan@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'muhammad nanda setiawan', 1, '085643252737', 'pati, 7 agustus 1992', 'FTMD', 1, '', '', '', '', ''),
('13310105', 'shanisafarah@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Shani Safarah Rohmah', 0, '087825909575', 'Serang, 22 Agustus 1992', 'FTI', 1, '', '', '', '', ''),
('13510005', 'aisyahdzulqaidah@yahoo.co.id', '21232f297a57a5a743894a0e4a801fc3', 'Aisyah Dzulqaidah', 0, '085780424034', 'Bandung, 27 April 1993', 'STEI', 1, '', '', '', '', ''),
('13511046', 'adilelfahmi@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Harits Elfahmi', 0, '', '', '', 0, '', '', '', '', ''),
('13610023', 'arrisandhi@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Putra Arri Sandhi', 1, '085695465665', 'Jakarta 2 Desember 1992', 'FTMD', 1, '', '', '', '', ''),
('13711052', 'anggahermawan@students.itb.ac.id', '21232f297a57a5a743894a0e4a801fc3', 'Angga Hermawan', 1, '+6289657394844', 'Serang,14 Juli 1993', 'FTMD', 1, '', '', '', '', ''),
('15010107', 'sriwulandariiq@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Sri Wulandari', 0, '08562122482', '28 April 1992', 'FTSL', 1, '', '', '', '', ''),
('15111052', 'rio.raharja@students.itb.ac.id', '21232f297a57a5a743894a0e4a801fc3', 'Rio Raharja', 1, '08573654275', 'Bandung 26 Desember 1994', 'FITB', 1, '', '', '', '', ''),
('15111082', 'ibrahimulin@students.itb.ac.id', '21232f297a57a5a743894a0e4a801fc3', 'Muchammad Ibrahim Ulinnuha', 1, '085641855839', 'Semarang, 26 Februari 1994', 'FITB', 1, '', '', '', '', ''),
('16012176', 'fanipus@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'fani puspitasari', 0, '087823761361', 'bandung, 30 mei 1994', 'FMIPA', 1, '', '', '', '', ''),
('16112054', 'zaainab92@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Zainab A Al-Ghazali', 0, '085720011421', 'Tasikmalaya, 05 Desember 1992', 'SITH', 1, '', '', '', '', ''),
('16112076', 'rika_mustika15@yahoo.co.id', '21232f297a57a5a743894a0e4a801fc3', 'rika mustika', 0, '085723669375', 'tasikmalaya 29 juni 1994', 'SITH', 1, '', '', '', '', ''),
('16112078', 'razanpaku@yahoo.co.id', '21232f297a57a5a743894a0e4a801fc3', 'Mhd. Fauzi Ramadhani Nasution', 1, '085372214871', '28 Januari 1994', 'SITH', 1, '', '', '', '', ''),
('16412280', 'saefulaziz919@yahoo.co.id', '21232f297a57a5a743894a0e4a801fc3', 'Saeful aziz', 1, '08562161202', 'Tasikmalaya 14 September 1994', 'FTTM', 1, '', '', '', '', ''),
('16512115', 'ekiafthar@yahoo.co.id', '21232f297a57a5a743894a0e4a801fc3', 'Eki Rizky Afthar', 1, '087722516675', '20 Maret 1994', 'STEI', 1, '', '', '', '', ''),
('16612306', 'kepakeksemua@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Andika Wiratama Suparto', 1, '085655359060', 'Lumajang 25 Agustus 1994', 'FTSL', 1, '', '', '', '', ''),
('16712224', 'sinagapilemon@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'pilemon sinaga', 1, '085351314963', 'Nagasaribu, 07 Oktober 1993', 'FTI', 1, '', '', '', '', ''),
('16812149', 'anggafauzan@yahoo.co.id', '21232f297a57a5a743894a0e4a801fc3', 'Angga Fauzan', 1, '085643743741', '17 April 1994', 'FSRD', 1, '', '', '', '', ''),
('17510024', 'elindaoktavia@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Elinda Oktavia', 0, '085723033349', 'Jakarta, 26 Oktober 1992', 'FSRD', 1, '', '', '', '', ''),
('181818181', 'hahaha@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'haha', 1, '0181818', '81181881', 'STEI', 0, '', '', '', '', ''),
('18209005', 'ddsasri@yahoo.com', '21232f297a57a5a743894a0e4a801fc3', 'Dyah Diwasasri', 0, '081234678', 'Semarang 29 September 1991', 'STEI', 0, '', '', '', '', ''),
('18209013', 'gharta.hadisa@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Gharta Hadisa Halim', 1, '085652039639', 'Rantau Prapat 21 November 1993', 'STEI', 1, '', '', '', '', ''),
('18209013(2)', 'gharta.hadisa@gmail.com2', '21232f297a57a5a743894a0e4a801fc3', 'gharta', 1, '085652039639', 'Bumi', 'STEI', 0, '', '', '', '', ''),
('182090133', 'gharta@gharta.com', '21232f297a57a5a743894a0e4a801fc3', 'gharta', 1, '1111', 'rantau', 'STEI', 0, '', '', '', '', ''),
('18209014', 'gharta.hadi2sa@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'gharta', 1, '085652039639', 'Rantau Prapat 21 11 1993', 'STEI', 0, '', '', '', '', ''),
('18209019', 'emreed1@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Ridwan', 0, '11', 'ttl Ridwan', 'STEI', 0, '', '', '', '', ''),
('19012169', 'dani.mustofa@sbm-itb.ac.id', '21232f297a57a5a743894a0e4a801fc3', 'Dani Mustofa', 1, '083873681757', '24 Desember 1994', 'SBM', 1, '', '', '', '', ''),
('2345456', 'pujitriutami@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Puji Tri Utami', 0, '085735036969', 'Ponorogo, 11 Agustus 1993', 'SBM', 1, '', '', '', '', ''),
('aaaa', 'gharta', '21232f297a57a5a743894a0e4a801fc3', '', 0, '', '', '', 1, '', '', '', '', ''),
('adminasrama', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 1, 'admin', 'Asrama ITB ', 'STEI', 1, 'admi', 'admin', 'admin', 'Rumah Orang Tua', 'admin');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `mulai_bayar`
--
ALTER TABLE `mulai_bayar`
  ADD CONSTRAINT `mulai_bayar_NID` FOREIGN KEY (`id_user`) REFERENCES `user` (`NID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
