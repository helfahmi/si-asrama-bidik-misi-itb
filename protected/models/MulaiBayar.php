<?php

/**
 * This is the model class for table "mulai_bayar".
 *
 * The followings are the available columns in table 'mulai_bayar':
 * @property integer $id
 * @property string $id_user
 * @property string $tanggal_mulai_bayar
 *
 * The followings are the available model relations:
 * @property User $idUser
 */
class MulaiBayar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mulai_bayar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, tanggal_mulai_bayar', 'required'),
			array('id_user', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, tanggal_mulai_bayar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idUser' => array(self::BELONGS_TO, 'User', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'User',
			'tanggal_mulai_bayar' => 'Tanggal Mulai Bayar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user,true);
		$criteria->compare('tanggal_mulai_bayar',$this->tanggal_mulai_bayar,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* Output: last unpaid month 
				kalo udah kebayar semua (atau menunggu konfirmasi) return current month
	*/
	public static function lastUnpaidMonth($id_user){
		/* Ambil bulan terawal yang belum dibayar oleh user dari awal (user.nid -> mulai_bayar.id_user -> mulai_bayar.tanggal_mulai_bayar */
		$bulan = date("Y-m", strtotime(MulaiBayar::model()->findByAttributes(array('id_user'=>$id_user))->tanggal_mulai_bayar));
		$bulan_akhir = date("Y-m");
		
		$month_should_be_paid = date("Y-m");
		$pembayarans = Pembayaran::model()->findAllByAttributes(array('user'=>$id_user));
		while(strtotime($bulan) <= strtotime($bulan_akhir)){
			/* Cari di tabel pembayaran row dengan bulan pembayaran sama dengan bulan $i */
			$found_paid = false;
			foreach($pembayarans as $bayar){
				if(date("Y-m", strtotime($bulan)) == date("Y-m", strtotime($bayar->bulan_pembayaran))){
					$found_paid = true;
					break;
				}
			}
			if(!$found_paid){
				$month_should_be_paid = $bulan;
				break;
			}
			$bulan = date ("Y-m", strtotime("+1 month", strtotime($bulan)));
		}
		
		return $month_should_be_paid."-01";
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MulaiBayar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
