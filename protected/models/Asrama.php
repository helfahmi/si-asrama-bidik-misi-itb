<?php

/**
 * This is the model class for table "asrama".
 *
 * The followings are the available columns in table 'asrama':
 * @property string $kode
 * @property string $nama
 * @property integer $jenis_kelamin
 * @property string $kontak
 * @property string $deskripsi
 * @property string $alamat
 * @property string $foto1
 * @property string $foto2
 * @property string $foto3
 * @property string $deskripsi_lengkap
 */
class Asrama extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'asrama';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode, nama, jenis_kelamin, kontak, deskripsi, alamat, foto1, deskripsi_lengkap', 'required'),
			array('jenis_kelamin', 'numerical', 'integerOnly'=>true),
			array('kode', 'length', 'max'=>32),
			array('nama, kontak, foto1, foto2, foto3', 'length', 'max'=>45),
			array('deskripsi', 'length', 'max'=>324),
			array('alamat', 'length', 'max'=>216),
			array('deskripsi_lengkap', 'length', 'max'=>10000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kode, nama, jenis_kelamin, kontak, deskripsi, alamat, foto1, foto2, foto3, deskripsi_lengkap', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kode' => 'Kode',
			'nama' => 'Nama',
			'jenis_kelamin' => 'Jenis Kelamin',
			'kontak' => 'Kontak',
			'deskripsi' => 'Deskripsi',
			'alamat' => 'Alamat',
			'foto1' => 'Foto1',
			'foto2' => 'Foto2',
			'foto3' => 'Foto3',
			'deskripsi_lengkap' => 'Deskripsi Lengkap',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin);
		$criteria->compare('kontak',$this->kontak,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('foto1',$this->foto1,true);
		$criteria->compare('foto2',$this->foto2,true);
		$criteria->compare('foto3',$this->foto3,true);
		$criteria->compare('deskripsi_lengkap',$this->deskripsi_lengkap,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Asrama the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
