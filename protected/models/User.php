<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $NID
 * @property string $email
 * @property string $password
 * @property string $nama
 * @property integer $jenis_kelamin
 * @property string $nomor_kontak
 * @property string $tempat_tanggal_lahir
 * @property string $fakultas_kode
 * @property integer $status
 * @property string $golongan_darah
 * @property string $alamat
 * @property string $penyakit
 * @property string $status_tinggal
 * @property string $alamat_tinggal
 * @property string $role
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NID, email, password, nama, jenis_kelamin, nomor_kontak, tempat_tanggal_lahir, fakultas_kode, status, golongan_darah, alamat, penyakit, status_tinggal, alamat_tinggal, role', 'required'),
			array('jenis_kelamin, status', 'numerical', 'integerOnly'=>true),
			array('NID, email, nomor_kontak, status_tinggal', 'length', 'max'=>32),
			array('password', 'length', 'max'=>128),
			array('nama', 'length', 'max'=>64),
			array('tempat_tanggal_lahir', 'length', 'max'=>216),
			array('fakultas_kode', 'length', 'max'=>45),
			array('golongan_darah', 'length', 'max'=>4),
			array('alamat, penyakit, alamat_tinggal', 'length', 'max'=>512),
			array('role', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('NID, email, password, nama, jenis_kelamin, nomor_kontak, tempat_tanggal_lahir, fakultas_kode, status, golongan_darah, alamat, penyakit, status_tinggal, alamat_tinggal, role', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'PresensiKegiatan'=> array(self::HAS_MANY, 'PresensiKegiatan', 'NID'),
		);
	}
	
	public function getEditTanggalMulaiButton(){
		//User::model()->findByAttributes(array('username'=>$this->username));
		//$id_mulai_bayar = MulaiBayar::model()->findByAttributes(array('id_user'=>$this->NID));
		return CHtml::link("Edit",array('editbayar','id'=>$this->NID));
	}
	
	public function getTanggalMulaiValue(){
		//echo "[NID: " . $this->NID . "]";
		//die();
		$model = MulaiBayar::model()->findByAttributes(array('id_user'=>$this->NID));
		$tanggal = $model['tanggal_mulai_bayar'];
		//echo "[$tanggal]";
		//die();
		
		return $tanggal;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'NID' => 'Nid',
			'email' => 'Email',
			'password' => 'Password',
			'nama' => 'Nama',
			'jenis_kelamin' => 'Jenis Kelamin',
			'nomor_kontak' => 'Nomor Kontak',
			'tempat_tanggal_lahir' => 'Tempat Tanggal Lahir',
			'fakultas_kode' => 'Fakultas Kode',
			'status' => 'Status',
			'golongan_darah' => 'Golongan Darah',
			'alamat' => 'Alamat',
			'penyakit' => 'Penyakit',
			'status_tinggal' => 'Status Tinggal',
			'alamat_tinggal' => 'Alamat Tinggal',
			'role' => 'Role',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('NID',$this->NID,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin);
		$criteria->compare('nomor_kontak',$this->nomor_kontak,true);
		$criteria->compare('tempat_tanggal_lahir',$this->tempat_tanggal_lahir,true);
		$criteria->compare('fakultas_kode',$this->fakultas_kode,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('golongan_darah',$this->golongan_darah,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('penyakit',$this->penyakit,true);
		$criteria->compare('status_tinggal',$this->status_tinggal,true);
		$criteria->compare('alamat_tinggal',$this->alamat_tinggal,true);
		$criteria->compare('role',$this->role,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
