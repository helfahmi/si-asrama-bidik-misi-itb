<?php

/**
 * This is the model class for table "tugas".
 *
 * The followings are the available columns in table 'tugas':
 * @property string $nim_mahasiswa
 * @property string $nama_mahasiswa
 * @property string $status
 * @property string $path
 */
class Tugas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tugas';
	}

	// YANG DIBAWAH BUAT UPLOAD TUGASNYA
	
	public $image;
    // ... other attributes
		
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nim_mahasiswa, nama_mahasiswa, status, file_name', 'required'),
			array('nim_mahasiswa', 'length', 'max'=>8),
			array('nama_mahasiswa', 'length', 'max'=>50),
			array('status', 'length', 'max'=>5),
			array('file_name', 'length', 'max'=>100),
			array('file_name', 'file', 'types'=>'jpg, gif, png, jpeg, bmp, zip, rar'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nim_mahasiswa, nama_mahasiswa, status, file_name', 'safe', 'on'=>'search'),
		);
		
		/* return array
		(
            array('image', 'file', 'types'=>'jpg, gif, png'),
        ); */
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nim_mahasiswa' => 'Nim Mahasiswa',
			'nama_mahasiswa' => 'Nama Mahasiswa',
			'status' => 'Status',
			'file_name' => 'File Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nim_mahasiswa',$this->nim_mahasiswa,true);
		$criteria->compare('nama_mahasiswa',$this->nama_mahasiswa,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('file_name',$this->file_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tugas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}
