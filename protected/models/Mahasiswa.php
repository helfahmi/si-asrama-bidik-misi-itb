<?php

/**
 * This is the model class for table "mahasiswa".
 *
 * The followings are the available columns in table 'mahasiswa':
 * @property string $nim_mahasiswa
 * @property string $nama_mahasiswa
 * @property string $letak_asrama
 * @property integer $no_kamar
 */
class Mahasiswa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mahasiswa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nim_mahasiswa, nama_mahasiswa, letak_asrama, no_kamar', 'required'),
			array('no_kamar', 'numerical', 'integerOnly'=>true),
			array('nim_mahasiswa', 'length', 'max'=>8),
			array('nama_mahasiswa', 'length', 'max'=>64),
			array('letak_asrama', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nim_mahasiswa, nama_mahasiswa, letak_asrama, no_kamar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nim_mahasiswa' => 'Nim Mahasiswa',
			'nama_mahasiswa' => 'Nama Mahasiswa',
			'letak_asrama' => 'Letak Asrama',
			'no_kamar' => 'No Kamar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nim_mahasiswa',$this->nim_mahasiswa,true);
		$criteria->compare('nama_mahasiswa',$this->nama_mahasiswa,true);
		$criteria->compare('letak_asrama',$this->letak_asrama,true);
		$criteria->compare('no_kamar',$this->no_kamar);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mahasiswa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
