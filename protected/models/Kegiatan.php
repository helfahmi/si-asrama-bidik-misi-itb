<?php

/**
 * This is the model class for table "kegiatan".
 *
 * The followings are the available columns in table 'kegiatan':
 * @property integer $id_kegiatan
 * @property string $nama_kegiatan
 * @property string $tgl_pelaksanaan
 * @property string $tempat_pelaksanaan
 * @property string $waktu_mulai
 * @property string $deskripsi_kegiatan
 * @property string $peserta
 * @property string $last_update
 */
class Kegiatan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kegiatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_kegiatan, tgl_pelaksanaan, tempat_pelaksanaan, waktu_mulai, deskripsi_kegiatan, peserta, last_update', 'required'),
			array('nama_kegiatan, tempat_pelaksanaan', 'length', 'max'=>64),
			array('deskripsi_kegiatan', 'length', 'max'=>256),
			array('peserta', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_kegiatan, nama_kegiatan, tgl_pelaksanaan, tempat_pelaksanaan, waktu_mulai, deskripsi_kegiatan, peserta, last_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'PresensiKegiatan'=> array(self::HAS_MANY, 'PresensiKegiatan', 'id_kegiatan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_kegiatan' => 'Id Kegiatan',
			'nama_kegiatan' => 'Nama Kegiatan',
			'tgl_pelaksanaan' => 'Tgl Pelaksanaan',
			'tempat_pelaksanaan' => 'Tempat Pelaksanaan',
			'waktu_mulai' => 'Waktu Mulai',
			'deskripsi_kegiatan' => 'Deskripsi Kegiatan',
			'peserta' => 'Peserta',
			'last_update' => 'Last Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_kegiatan',$this->id_kegiatan);
		$criteria->compare('nama_kegiatan',$this->nama_kegiatan,true);
		$criteria->compare('tgl_pelaksanaan',$this->tgl_pelaksanaan,true);
		$criteria->compare('tempat_pelaksanaan',$this->tempat_pelaksanaan,true);
		$criteria->compare('waktu_mulai',$this->waktu_mulai,true);
		$criteria->compare('deskripsi_kegiatan',$this->deskripsi_kegiatan,true);
		$criteria->compare('peserta',$this->peserta,true);
		$criteria->compare('last_update',$this->last_update,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kegiatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
