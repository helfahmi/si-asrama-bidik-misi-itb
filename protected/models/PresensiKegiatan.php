<?php

/**
 * This is the model class for table "presensi_kegiatan".
 *
 * The followings are the available columns in table 'presensi_kegiatan':
 * @property integer $id_presensi
 * @property integer $id_kegiatan
 * @property string $nim_mahasiswa
 * @property string $kehadiran
 */
class PresensiKegiatan extends CActiveRecord
{


	public $nama_search;
	public $nama_kegiatan;
	/**
	 * @return string the associated database table name
	 */
	 
	 
	public function tableName()
	{
		return 'presensi_kegiatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_presensi, id_kegiatan, nim_mahasiswa, kehadiran', 'required'),
			array('id_presensi, id_kegiatan', 'numerical', 'integerOnly'=>true),
			array('nim_mahasiswa', 'length', 'max'=>8),
			array('kehadiran', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_presensi, id_kegiatan, nim_mahasiswa, kehadiran,nama_search,nama_kegiatan', 'safe', 'on'=>'search'),
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'User'=>array(self::BELONGS_TO, 'User', 'nim_mahasiswa'),
			'Kegiatan'=>array(self::BELONGS_TO, 'Kegiatan', 'id_kegiatan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_presensi' => 'Id Presensi',
			'id_kegiatan' => 'Id Kegiatan',
			'nim_mahasiswa' => 'NIM',
			'kehadiran' => 'Hadir',
			'nama_search' => 'Nama',
			'nama_kegiatan'=>'Nama Kegiatan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array( 'User','Kegiatan');
		$criteria->compare('id_presensi',$this->id_presensi);
		$criteria->compare('t.id_kegiatan',$this->id_kegiatan);
		$criteria->compare('t.nim_mahasiswa',$this->nim_mahasiswa,true);
		$criteria->compare('kehadiran',$this->kehadiran,true);
		$criteria->compare( 'User.nama', $this->nama_search, true );
		$criteria->compare( 'Kegiatan.nama_kegiatan', $this->nama_kegiatan, true );
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
			'attributes'=>array(
            'nama_search'=>array(
                'asc'=>'User.nama',
                'desc'=>'User.nama DESC',
            ),
			  'nama_kegiatan'=>array(
                'asc'=>'Kegiatan.nama_kegiatan',
                'desc'=>'Kegiatan.nama_kegiatan DESC',
            ),
            '*',
        ),
    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return presensiKegiatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
