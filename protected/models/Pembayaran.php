<?php

/**
 * This is the model class for table "pembayaran".
 *
 * The followings are the available columns in table 'pembayaran':
 * @property integer $id
 * @property string $user
 * @property integer $nominal
 * @property string $bulan_pembayaran
 * @property string $tanggal
 * @property string $bukti
 * @property string $status
 */
class Pembayaran extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pembayaran';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user, nominal, tanggal, bulan_pembayaran, bukti, keterangan', 'required'),
			array('nominal', 'numerical', 'integerOnly'=>true),
			array('user', 'length', 'max'=>50),
			array('bukti', 'length', 'max'=>100),
			array('keterangan', 'length', 'max'=>100),
			array('status', 'length', 'max'=>10),
			array('bukti', 'file', 'types'=>'jpg, gif, png, jpeg, bmp, zip, rar'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user, nominal, tanggal, bukti, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user' => 'User',
			'nominal' => 'Nominal',
			'tanggal' => 'Tanggal',
			'bulan_pembayaran' => 'Bulan Pembayaran',
			'bukti' => 'Bukti',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user',$this->user,true);
		$criteria->compare('nominal',$this->nominal);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('bulan_pembayaran',$this->bulan_pembayaran,true);
		$criteria->compare('bukti',$this->bukti,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getFormattedBukti(){
		$length = strlen($this->bukti);
		if($length > 12){
			$label = "...".substr($this->bukti, $length - 13, 13);
		} else {
			$label = $this->bukti;
		}
		
		return CHtml::link($label, array('/uploads/bukti/'.$this->bukti), array('target'=>'_blank'));
	}	
	public function getFormattedNominal(){
		return "Rp ".$this->nominal;
	}
	
	public function getFormattedDate(){
		return date("d-m-Y", strtotime($this->tanggal));
	}

	public function getFormattedBayarDate(){
		return date("F Y", strtotime($this->bulan_pembayaran));
	}
	
	public function getOperationRights(){
		$rights = ($this->status == "Belum" ? '{view}{delete}' : '{view}');
		return $rights;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pembayaran the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
