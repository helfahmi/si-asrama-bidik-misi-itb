<?php

class ChangePasswordForm extends CFormModel
{
	public $currentPassword;
	public $newPassword;
	public $newPassword_repeat;
	private $_user;
	
	public function rules()
	{
		return array(
			array(
				'currentPassword', 'compareCurrentPassword'
			),
			array(
				'currentPassword, newPassword, newPassword_repeat', 'required',
				'message' => 'not blank',
			),
			array(
				'newPassword_repeat', 'compare',
				'compareAttribute'=>'newPassword',
				'message'=>'new password not same',
			),
			
		);
	}
	
	public function compareCurrentPassword($attribute,$params)
	{
		if(md5($this->currentPassword) !== $this->_user->password)
		{
			$this->addError($attribute,'wrong password');
		}
	}
	
	public function init()
	{
		
			$this->_user = User::model()->findByAttributes(array('email'=>Yii::app()->user->name));
	
	}
	
	public function attributeLabels()
	{
		return array(
		'currentPassword'=>'Current Password',
		'newPassword'=>'New Password',
		'newPassword_repeat'=>'New Password (Repeat)',
		);
	}
	
	public function changePassword()
	{

		$this->_user->password=MD5($this->newPassword);
		if($this->_user->save())
			return true;
			print_r($this->_user->getErrors());
		return false;
		
	}
	
}