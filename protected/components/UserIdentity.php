<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	 
	public static $status_error = 0;
	public function authenticate()
	{
		self::$status_error = 0;
		$user=User::model()->findByAttributes(array('email'=>$this->username));
		
		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if($user->password != md5($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else {
			if ($user->role==='mahasiswa'){
				$this->setState('role','mahasiswa');
				$this->setState('nama',$user->nama);
				$this->setState('nim',$user->NID);
			}elseif($user->role==='tutor'){
				$this->setState('role','tutor');
				$this->setState('nama',$user->nama);
				$this->setState('nim',$user->NID);
			}elseif($user->role==='koordinator'){
				$this->setState('role','koordinator');
			}
			$this->setState('nama', $user->nama);
			$this->setState('nid', $user->NID);
			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}
}