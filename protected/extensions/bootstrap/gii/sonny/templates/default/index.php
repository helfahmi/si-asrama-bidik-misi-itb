<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php $label=$this->class2name($this->modelClass); ?>
<h1><?php echo $label; ?></h1>

<?php echo "\t<?php echo CHtml::link(\"Create\",array('create')); ?>\n\t<br />\n\n"; ?>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
