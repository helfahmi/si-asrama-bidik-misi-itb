<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<div>

<?php
echo "\t<?php echo CHtml::link(\$data->{$this->tableSchema->primaryKey},array('view','id'=>\$data->{$this->tableSchema->primaryKey})); ?>\n\t<br />\n\n";
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if($column->isPrimaryKey)
		continue;
	if(++$count==7)
		echo "\t<?php /*\n";
	echo "\t<?php echo \$data->{$column->name}; ?>\n\t<br />\n\n";

}
if($count>=7)
	echo "\t*/ ?>\n";

echo "\t<?php echo CHtml::link(\"Update\",array('update','id'=>\$data->{$this->tableSchema->primaryKey})); ?>\n\t<br />\n\n";
echo "\t<?php echo CHtml::link(\"Delete\",array('delete','id'=>\$data->{$this->tableSchema->primaryKey})); ?>\n\t<br />\n\n";
?>

</div>