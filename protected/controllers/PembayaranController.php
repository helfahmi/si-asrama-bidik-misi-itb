<?php

class PembayaranController extends Controller
{
	public $layout='//layouts/main_pembayaran';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('*'),
				'users'=>array('@'),
			),
			array('deny',
                'users'=>array('?'),
            ),
		);
	}
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	
	public function actionLaporan(){
	
		if(isset($_GET['month']))
            $month=$_GET['month'];
        else
            $month=date("m");
 
        if(isset($_GET['year']))
            $year=$_GET['year'];
        else
            $year=date("Y");
		
		$count=Yii::app()->db->createCommand("SELECT COUNT(*) FROM user")->queryScalar();
		$sql="	SELECT	u.NID AS nim,
						u.nama AS nama,
						p.tanggal AS tanggal,
						p.status AS status
				FROM pembayaran p
				LEFT JOIN user u
				ON p.user=u.NID
				WHERE p.bulan_pembayaran LIKE '".$year."-".$month."-__'
				UNION
				SELECT	u.NID AS nim,
						u.nama AS nama,
						NULL AS tanggal,
						NULL AS status
				FROM user u
				WHERE u.NID NOT IN (
					SELECT	u.NID AS nim
					FROM pembayaran p
					LEFT JOIN user u
					ON p.user=u.NID
					WHERE p.bulan_pembayaran LIKE '".$year."-".$month."-__'
				)";
						
		$dataProviderr=new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 'nama', 'tanggal', 'status', 'nim',
				),
			),
			'pagination'=>array(
				'pageSize'=>$count,
			),
			'keyField'=>'nim',
		));
		
		$nBelumBayar=0;
		$nSudahBayarBelumDiverifikasi=0;
		$nSudahBayarSudahDiverifikasi=0;
		
		foreach($dataProviderr->getData() as $record) {
			if (!isset($record["status"])) {
				$nBelumBayar++;
			}
			else if ($record["status"] == "Belum") {
				$nSudahBayarBelumDiverifikasi++;
			}
			else {
				$nSudahBayarSudahDiverifikasi++;
			}
		}
		
		$dataProvider=new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 'nama', 'tanggal', 'status', 'nim',
				),
			),
			'pagination'=>array(
				'pageSize'=>10,
			),
			'keyField'=>'nim',
		));
		
		$this->render('laporan',array(
			'dataProvider'=>$dataProvider,
			'month'=>$month,
			'year'=>$year,
			'nBelumBayar'=>$nBelumBayar,
			'nSudahBayarBelumDiverifikasi'=>$nSudahBayarBelumDiverifikasi,
			'nSudahBayarSudahDiverifikasi'=>$nSudahBayarSudahDiverifikasi,
		));
	}
	
	public function actionManageUser(){
		if(Yii::app()->user->getState('nid') != 0){
			$this->redirect(array('pembayaran/index'));
		}
		
		$criteria = new CDbCriteria;
		$criteria->select='t.*, mulai_bayar.*';
		$criteria->join='LEFT JOIN mulai_bayar ON mulai_bayar.id_user = t.NID';
		
		$dataProvider = new CActiveDataProvider('user', 
			array('criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 10,
			),
		));
		
		$this->render('manage',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionEditBayar($id){
		if(Yii::app()->user->getState('nid') != 0){
			$this->redirect(array('pembayaran/index'));
		}
		$model = MulaiBayar::model()->findByAttributes(array('id_user'=>$id));
		if($model == null){
			$model=new MulaiBayar;
			$model->id_user = $id;
		}
		// uncomment the following code to enable ajax-based validation
		/*
		if(isset($_POST['ajax']) && $_POST['ajax']==='mulai-bayar-edit_bayar-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		*/

		if(isset($_POST['MulaiBayar']))
		{
			$model->attributes=$_POST['MulaiBayar'];
			if($model->save())
			{
				// form inputs are valid, do something here
				$this->redirect(array('pembayaran/manageuser'));
				return;
			}
		}
		$this->render('edit_bayar',array('model'=>$model));
	}
	
	public function actionStatus(){
		$criteria=new CDbCriteria(array(                    
			'condition'=>'user='.Yii::app()->user->getState('nid'),
		));
		$dataProvider=new CActiveDataProvider('Pembayaran', array(
			'criteria'=>$criteria,
		));
		$this->render('status',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionAdmin(){
		if(Yii::app()->user->getState('nid') != 0){
			$this->redirect(array('pembayaran/index'));
		}
		$dataProvider=new CActiveDataProvider('Pembayaran');
		$this->render('admin',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionCreate()
	{
		$model=new Pembayaran;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pembayaran']))
		{
			$model->attributes=$_POST['Pembayaran'];
			$model->bukti=CUploadedFile::getInstance($model,'bukti');
			$model->tanggal=date("Y-m-d");
			$model->user = Yii::app()->user->getState('nid');
			$model->keterangan = "N/A";
			
			
			$model->bulan_pembayaran = MulaiBayar::model()->lastUnpaidMonth($model->user);
			
			if($model->validate()){
				$bukti_unik = uniqid();
				$model->bukti->saveAs('uploads/bukti/'. $bukti_unik.$model->bukti);
				$model->bukti = $bukti_unik.$model->bukti;
				$model->save();
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('_form',array(
			'title'=>'Buat Pembayaran Baru',
			'model'=>$model,
		));
	}
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pembayaran']))
		{
			$model->attributes=$_POST['Pembayaran'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('_form',array(
			'title'=>'Update',
			'model'=>$model,
		));
	}
	public function actionDelete($id)
	{
		
		//Kalau file bukti pembayaran sudah tidak ada, lewati
		if (file_exists('uploads/bukti/'. $this->loadModel($id)->bukti)) {
			unlink('uploads/bukti/'. $this->loadModel($id)->bukti);
		} 
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}
	
	public function actionUpdateEditable(){
		$es = new EditableSaver('Pembayaran');  //'User' is name of model to be updated
		$es->update();
	}
	
	/* Melakukan verifikasi pembayaran */
	public function actionVerify(){
		if(Yii::app()->user->getState('nid') != 0){
			$this->redirect(array('pembayaran/index'));
		}
		
		$model = new Pembayaran('search');
		$model->unsetAttributes();
		
		if(isset($_GET['Pembayaran'])){
            $model->attributes=$_GET['Pembayaran'];
			if($model->bulan_pembayaran != ""){
				$model->bulan_pembayaran = date("Y-m", strtotime($model->bulan_pembayaran));
			}
		}
		
		$this->render('verify', array(
				'model'=>$model,
		));
			
		/*
		$dataProvider=new CActiveDataProvider('Pembayaran');
		$this->render('verify',array(
			'dataProvider'=>$dataProvider,
		));
		*/
	}
	
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pembayaran');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	public function loadModel($id)
	{
		$model=Pembayaran::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pembayaran-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
