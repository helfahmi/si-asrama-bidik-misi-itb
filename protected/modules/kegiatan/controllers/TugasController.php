<?php

class TugasController extends Controller
{
	public $layout='/layouts/column1';
	
	public function actionIndex()
	{
		$model=new Tugas('search');
		$dataProvider=new CActiveDataProvider('Tugas');
		
		//	KALO TUTOR, RENDER PAGE NYA BEDA 
		
		if (Yii::app()->user->getState("role")=="tutor")
		{
			$this->render('lihattugas',array('model'=>$model));
		}
		else
		{
			$this->render('index',array('model'=>$model));
		}
	}
	
	public function actionView($id=null)
	{
		//	BEDA ROLE BEDA PAGE 
		
		if(Yii::app()->user->getState("role")=="mahasiswa")
		{
			if($id===null)
				throw new CHttpException(404,'The requested page does not exist.');
			$model=new Tugas('search');
			
			$model->unsetAttributes();  // clear any default values
			
			if(isset($_GET['Tugas']))
				$model->attributes=$_GET['Tugas'];
				
			$namakegiatan=Kegiatan::model()->findByPk($id);
		
			$dataProvider = $model->search();
			$dataProvider->criteria->addCondition('t.id_kegiatan='.$id);
			//$model=$this->loadModelAttr($id);
		
			$this->render('index',array(
				'model'=>$model,'dataProvider'=>$dataProvider,'namakegiatan'=>$namakegiatan,'id'=>$id,
			));
		}
		else
		{
			if($id===null)
				throw new CHttpException(404,'The requested page does not exist.');
			$model=new Tugas('search');
			
			$model->unsetAttributes();  // clear any default values
			
			if(isset($_GET['Tugas']))
				$model->attributes=$_GET['Tugas'];
				
			$namakegiatan=Kegiatan::model()->findByPk($id);
		
			$dataProvider = $model->search();
			$dataProvider->criteria->addCondition('t.id_kegiatan='.$id);
			//$model=$this->loadModelAttr($id);
		
			$this->render('lihattugas',array(
				'model'=>$model,'dataProvider'=>$dataProvider,'namakegiatan'=>$namakegiatan,'id'=>$id,
			));
		}
	}
	
	public function loadModel($id)
	{
		$model=Tugas::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	//	YANG DIBAWAH BUAT UPLOAD TUGAS NYA
	
	public function actionCreate()
    {
        $model=new Tugas;
		
        if(isset($_POST['Tugas']))
        {
            $model->attributes=$_POST['Tugas'];
			$model->file_name=CUploadedFile::getInstance($model,'file_name');
			$model->nama_mahasiswa=Yii::app()->user->getState('nama');
			$model->nim_mahasiswa=Yii::app()->user->getState('nim');
			$model->status="Sudah";
			$model->id_kegiatan=$_POST['id'];
			
			// SAVE DAN MUNCULIN ALERT KETIKA SUDAH BERHASIL SAVE
			
			if($model->save())
			{
				$model->file_name->saveAs('uploads/tugas/'. $model->file_name);
				//$this->redirect(array('view','id'=>$model->id));
				
				Yii::app()->user->setFlash('success', '<center><strong>Selamat, '.Yii::app()->user->getState('nama').'  !</strong> Tugas Anda sudah berhasil diunggah.</center>');
				Yii::app()->user->setFlash('info', '<strong>Heads up!</strong> This alert needs your attention, but it\'s not super important.');

				$this->widget('bootstrap.widgets.TbAlert', array(
						'block'=>true, // display a larger alert block?
						'fade'=>true, // use transitions?
						'closeText'=>false,
						'alerts'=>array( // configurations per alert type
							'success'=>array('block'=>true, 'fade'=>true),// success, info, warning, error or danger
						),
				)); 
			}
			else
			{
				Yii::app()->user->setFlash('info', '<strong>Heads up!</strong> This alert needs your attention, but it\'s not super important.');

				$this->widget('bootstrap.widgets.TbAlert', array(
						'block'=>true, // display a larger alert block?
						'fade'=>true, // use transitions?
						'closeText'=>false,
						'alerts'=>array( // configurations per alert type
							'error'=>array('block'=>true, 'fade'=>true),// success, info, warning, error or danger
						),
				)); 
			}
		  
			$this->render('index', array('model'=>$model,'id'=>$_POST['id']));	
        }
      
    }
	
	//	BUAT DOWNLOAD NYA
	
	public function actionDownload($name)
	{
		$filecontent=file_get_contents('uploads/tugas/'.$model->file_name);
		header("Content-Type: text/plain");
		header("Content-disposition: attachment; filename=$model->file_name");
		header("Pragma: no-cache");
		echo $filecontent;
		exit;
	}
	
	// public function actionDownload()
	// {
		// $model = new Download; 
		// $name = $_GET['file'];	
		// $upload_path = Yii::app()->params['uploadPath'];	

		// if( file_exists( $upload_path.$name ) )
		// {
			// Yii::app()->getRequest()->sendFile( $name , file_get_contents( $upload_path.$name ) );
		// }
		// else
		// {
			// $this->render('download404');
		// }	
	// }	 



	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}