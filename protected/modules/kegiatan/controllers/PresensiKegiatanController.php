<?php

class PresensiKegiatanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
	
		if (Yii::app()->user->getState("role")=="tutor")
			$arr=array('partupdate');
		else
			$arr=array('');
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>$arr,
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id=null)
	{
		if($id===null)
			throw new CHttpException(404,'The requested page does not exist.');
		$model=new PresensiKegiatan('search');
		
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['PresensiKegiatan']))
			$model->attributes=$_GET['PresensiKegiatan'];
			
		$namakegiatan=Kegiatan::model()->findByPk($id);
	
		$dataProvider = $model->search();
		$dataProvider->criteria->addCondition('t.id_kegiatan='.$id);
		//$model=$this->loadModelAttr($id);
	
		$this->render('index',array(
			'model'=>$model,'dataProvider'=>$dataProvider,'namakegiatan'=>$namakegiatan,
		));
	}


	/**
	 * Lists all models.
	 */
	public function actionIndex($id=null)
	{
		if($id===null)
			throw new CHttpException(404,'The requested page does not exist.');
		$model=new PresensiKegiatan('search');
		
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['PresensiKegiatan']))
			$model->attributes=$_GET['PresensiKegiatan'];
			
		$namakegiatan=Kegiatan::model()->findByPk($id);
	
		$dataProvider = $model->search();
		$dataProvider->criteria->addCondition('t.id_kegiatan='.$id);
		//$model=$this->loadModelAttr($id);
	
		$this->render('index',array(
			'model'=>$model,'dataProvider'=>$dataProvider,'namakegiatan'=>$namakegiatan,
		));
	}

	
	public function actionPartUpdate($id = null) {
			
       $model = $this->loadModel($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
		if($model->kehadiran==='yes')
			$model->kehadiran="no";
		else
			$model->kehadiran="yes";
		$model->save();
		
		
	/*	$namakegiatan=Kegiatan::model()->findByPk(3);

		$model=new AbsensiKegiatan('search');


		
		$dataProvider = $model->search();
        $this->renderPartial('admin', array('model' => $model,'dataProvider'=>$dataProvider,'namakegiatan'=>namakegiatan));
        Yii::app()->end();*/
}
	
	

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AbsensiKegiatan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PresensiKegiatan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	/**
	 * Performs the AJAX validation.
	 * @param AbsensiKegiatan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='presensi-kegiatan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
