<?php

class LaporanController extends Controller
{
	public function actionIndex()
	{
		//$this->render('index');
		$mPDF1 = Yii::app()->ePdf->mpdf();
		$stylesheet1 = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/print.css');
		$mPDF1->WriteHTML($stylesheet1, 1);
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/laporan.css');
		$mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('index', array(), true));
        $mPDF1->Output();
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}