<?php

class KegiatanController extends Controller
{
	public $layout='column1';

	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionKonfirmasi()
	{
		$this->render('konfirmasi');
	}
	
	public function actionConfirm($id=null)
	{
		if($id===null)
			throw new CHttpException(404,'The requested page does not exist.');
		
		$previous = AjuanKegiatan::model()->findByPk((int)$id);
		$target = new Kegiatan;
		//$target->id_kegiatan = $previous->id_kegiatan;
		$target->nama_kegiatan = $previous->nama_kegiatan;
		$target->tgl_pelaksanaan = $previous->tgl_pelaksanaan;
		$target->tempat_pelaksanaan = $previous->tempat_pelaksanaan;
		$target->waktu_mulai = $previous->waktu_mulai;
		$target->deskripsi_kegiatan = $previous->deskripsi_kegiatan;
		$target->peserta = $previous->peserta;
		$target->last_update = date('Y-m-d G:i:s');
		
		if($target->save())
		{
			// data is valid and is successfully inserted/updated
			$rows = explode(",",$target->peserta);
			foreach($rows as $row)
			{
				$penghunis = Yii::app()->db->createCommand()->select()->from('menghuni')->where('id_asrama=:id', array(':id'=>$row))->queryAll();
				foreach($penghunis as $penghuni)
				{
					if(Yii::app()->db->createCommand()->select()->from('user')->where('NID=:id', array(':id'=>$penghuni['NID']))->queryRow()['role'] == "mahasiswa")
					{
						$command = Yii::app()->db->createCommand();
						$command->insert('presensi_kegiatan', array(
							'id_kegiatan'=>''.$target->id_kegiatan,
							'nim_mahasiswa'=>''.$penghuni['NID'],
							'kehadiran'=>'no',
						));
						
						$command = Yii::app()->db->createCommand();
						$command->insert('tugas', array(
							'id_kegiatan'=>''.$target->id_kegiatan,
							'nim_mahasiswa'=>''.$penghuni['NID'],
							'nama_mahasiswa'=>''.Yii::app()->db->createCommand()->select()->from('user')->where('NID=:id', array(':id'=>$penghuni['NID']))->queryRow()['nama'],
							'status'=>'belum',
							'file_name'=>'null',
						));
					}
				}
			}
			$previous->delete();
			$this->redirect(array("/kegiatan"));
		}
		else
		{
			// data is invalid. call getErrors() to retrieve error messages
			$this->render('konfirmasi');
		}
	}
	
	public function actionBuat()
	{
		$model=new Kegiatan; 

		// uncomment the following code to enable ajax-based validation 
		/* 
		if(isset($_POST['ajax']) && $_POST['ajax']==='kegiatan-buat_kegiatan-form')
		{ 
			echo CActiveForm::validate($model); 
			Yii::app()->end(); 
		} 
		*/ 

		if(isset($_POST['Kegiatan'])) 
		{ 
			$model->attributes=$_POST['Kegiatan'];
			//$model->peserta = implode(",", $model->asramaIds);
			$model->last_update = date('Y-m-d G:i:s');
			
			$text="";
			$j = 0;
			for($i = 0; $i < $_POST['counter']; $i++)
			{
				if(isset($_POST[''.$i]))
				{
					if($j == 0)
					{
						$text .= $_POST[''.$i];
						$j++;
					}
					else
					{
						$text .= ",".$_POST[''.$i];
						$j++;
					}
				}
			}
			
			$model->peserta=$text;
			
			if($model->save()) 
			{ 
				// form inputs are valid, do something here 
				$rows = explode(",",$model->peserta);
				foreach($rows as $row)
				{
					$penghunis = Yii::app()->db->createCommand()->select()->from('menghuni')->where('id_asrama=:id', array(':id'=>$row))->queryAll();
					foreach($penghunis as $penghuni)
					{
						if(Yii::app()->db->createCommand()->select()->from('user')->where('NID=:id', array(':id'=>$penghuni['NID']))->queryRow()['role'] == "mahasiswa")
						{
							$command = Yii::app()->db->createCommand();
							$command->insert('presensi_kegiatan', array(
								'id_kegiatan'=>''.$model->id_kegiatan,
								'nim_mahasiswa'=>''.$penghuni['NID'],
								'kehadiran'=>'no',
							));
							
							$command = Yii::app()->db->createCommand();
							$command->insert('tugas', array(
								'id_kegiatan'=>''.$model->id_kegiatan,
								'nim_mahasiswa'=>''.$penghuni['NID'],
								'nama_mahasiswa'=>''.Yii::app()->db->createCommand()->select()->from('user')->where('NID=:id', array(':id'=>$penghuni['NID']))->queryRow()['nama'],
								'status'=>'belum',
								'file_name'=>'null',
							));
						}
					}
				}
				$this->redirect(array("/kegiatan"));
				return;  
			} 
		} 
		$this->render('buat_kegiatan',array('model'=>$model));
	}

	public function actionForm_ajuan()
	{
		$model=new AjuanKegiatan; 

		// uncomment the following code to enable ajax-based validation 
		/* 
		if(isset($_POST['ajax']) && $_POST['ajax']==='ajuan-kegiatan-form_ajuan-form') 
		{ 
			echo CActiveForm::validate($model); 
			Yii::app()->end(); 
		} 
		*/ 

		if(isset($_POST['AjuanKegiatan'])) 
		{ 
			$model->attributes=$_POST['AjuanKegiatan'];
			
			//$model->peserta = implode(",", $model->asramaIds);
			$model->pengaju = Yii::app()->user->getState("nama"); //Ganti pake nama mahasiswa atau tutor dari user identity
			$model->last_update = date('Y-m-d G:i:s');
			
			$text="";
			$j = 0;
			for($i = 0; $i < $_POST['counter']; $i++)
			{
				if(isset($_POST[''.$i]))
				{
					if($j == 0)
					{
						$text .= $_POST[''.$i];
						$j++;
					}
					else
					{
						$text .= ",".$_POST[''.$i];
						$j++;
					}
				}
			}
			
			$model->peserta=$text;
			if($model->save()) 
			{ 
				// form inputs are valid, do something here
				$this->redirect(array("/kegiatan"));
				return; 
			} 
		} 
		$this->render('form_ajuan',array('model'=>$model));
	}
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}