<?php

class PasswordController extends Controller
{
	/*public function filters()
	{
		return array(
			'accesControl',
		);
	}*/
	public $layout='/layouts/column1';
	
	public function accesRules()
	{
		return array(
			array(
			'deny',
			'actions'=>array('ChangePassword'),
			'user'=>array('?'),
			'expression'=>"Yii::app()->params['authSystem']['type'] === 'MySqlUserIdentity'",
			),
		);
	}
	
	public function actionChangePassword()
	{
		$model = new ChangePasswordForm;
		if(isset($_POST['ajax']) && $_POST['ajax']==='changePassword-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		if(isset($_POST['ChangePasswordForm']))
		{
			$model->attributes=$_POST['ChangePasswordForm'];
			if ($model->validate() && $model->changePassword())
			{
				Yii::app()->user->setFlash('success','<b><i>Password has been changed</i></b>');
							$this->redirect($this->action->id);

				//
			}else

			{
				Yii::app()->user->setFlash('success','<b><i>WOWA</i></b>');
			}
		}
		$this->render('changePassword',array('model'=>$model));
	}
}
	