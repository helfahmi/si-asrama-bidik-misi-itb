<?php
/* @var $this LaporanController */

$this->breadcrumbs=array(
	'Laporan',
);
?>
<!--
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<p>
	You may change the content of this page by modifying
	the file <tt><?php echo __FILE__; ?></tt>.
</p>
-->
<h1>Laporan Kegiatan Pembinaan Asrama ITB</h1>
<h4>(Versi <?php echo date('d M Y G:i:s');?>)</h4>
<hr>
<!-- Rincian Kegiatan Pembinaan -->
<h3>1. Rincian Kegiatan Pembinaan</h3>
<?php
	$rows = Yii::app()->db->createCommand()->select()->from('kegiatan')->order('last_update desc')->queryAll();
	$rows = array_filter($rows);
	if(!empty($rows))
	{
		echo "<table>
				<tr>
					<th>Nama Kegiatan</th>
					<th>Tanggal Kegiatan</th>
					<th>Deskripsi</th>
				</tr>";
			foreach($rows as $row)
			{
				echo "<tr>";
					echo "<td>" . $row['nama_kegiatan'] . "</td>";
					echo "<td>" . $row['tgl_pelaksanaan'] . "</td>";
					echo "<td>" . $row['deskripsi_kegiatan'] . "</td>";
				echo "</tr>";
			}
		echo "</table>";
	}
	else
	{
		echo "<h4>Belum ada kegiatan</h4>";
	}
?>
<br>
<!-- Rincian Kehadiran Mahasiswa -->
<h3>2. Rincian Kehadiran Mahasiswa</h3>
<?php
	$rows = Yii::app()->db->createCommand()->select()->from('kegiatan')->order('last_update desc')->queryAll();
	$rows = array_filter($rows);
	if(!empty($rows))
	{
		echo "<table>
				<tr>
					<th>Nama Kegiatan</th>
					<th>Target Peserta</th>
					<th>Presentase Kehadiran</th>
				</tr>";
			foreach($rows as $row)
			{
				$list = Yii::app()->db->createCommand()->select()->from('presensi_kegiatan')->where('id_kegiatan=:id', array(':id'=>$row['id_kegiatan']))->queryAll();
				$list = array_filter($list);
				$count = 0;
				$hadir = 0;
				foreach($list as $item)
				{
					$count++;
					if($item['kehadiran'] == "yes")
					{
						$hadir++;
					}
				}
				echo "<tr>";
					echo "<td>" . $row['nama_kegiatan'] . "</td>";
					echo "<td>" . $count . "</td>";
					echo "<td>" . $hadir/$count*100 . "%</td>";
				echo "</tr>";
			}
		echo "</table>";
	}
	else
	{
		echo "<h4>Belum ada kegiatan</h4>";
	}
?>
<br>
<!-- Mahasiswa Bermasalah -->
<h3>3. Mahasiswa Bermasalah</h3>
<p>Belum ada kesepakatan untuk menentukan kriteria seperti apa yang disebut dengan <b>mahasiswa bermasalah</b>.</p>