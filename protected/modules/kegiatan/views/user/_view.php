<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('NID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->NID), array('view', 'id'=>$data->NID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_kontak')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_kontak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_tanggal_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_tanggal_lahir); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fakultas_kode')); ?>:</b>
	<?php echo CHtml::encode($data->fakultas_kode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('golongan_darah')); ?>:</b>
	<?php echo CHtml::encode($data->golongan_darah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penyakit')); ?>:</b>
	<?php echo CHtml::encode($data->penyakit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_tinggal')); ?>:</b>
	<?php echo CHtml::encode($data->status_tinggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_tinggal')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_tinggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('role')); ?>:</b>
	<?php echo CHtml::encode($data->role); ?>
	<br />

	*/ ?>

</div>