<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'NID'); ?>
		<?php echo $form->textField($model,'NID',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'NID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jenis_kelamin'); ?>
		<?php echo $form->textField($model,'jenis_kelamin'); ?>
		<?php echo $form->error($model,'jenis_kelamin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nomor_kontak'); ?>
		<?php echo $form->textField($model,'nomor_kontak',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'nomor_kontak'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tempat_tanggal_lahir'); ?>
		<?php echo $form->textField($model,'tempat_tanggal_lahir',array('size'=>60,'maxlength'=>216)); ?>
		<?php echo $form->error($model,'tempat_tanggal_lahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fakultas_kode'); ?>
		<?php echo $form->textField($model,'fakultas_kode',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'fakultas_kode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'golongan_darah'); ?>
		<?php echo $form->textField($model,'golongan_darah',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'golongan_darah'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alamat'); ?>
		<?php echo $form->textField($model,'alamat',array('size'=>60,'maxlength'=>512)); ?>
		<?php echo $form->error($model,'alamat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'penyakit'); ?>
		<?php echo $form->textField($model,'penyakit',array('size'=>60,'maxlength'=>512)); ?>
		<?php echo $form->error($model,'penyakit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status_tinggal'); ?>
		<?php echo $form->textField($model,'status_tinggal',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'status_tinggal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alamat_tinggal'); ?>
		<?php echo $form->textField($model,'alamat_tinggal',array('size'=>60,'maxlength'=>512)); ?>
		<?php echo $form->error($model,'alamat_tinggal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'role'); ?>
		<?php echo $form->textField($model,'role',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'role'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->