<?php
/* @var $this AbsensiKegiatanController */
/* @var $model AbsensiKegiatan */
/*$this->breadcrumbs=array(
	'Absensi Kegiatans'=>array('index'),
	'Manage',
);*/
/*
$this->menu=array(
	array('label'=>'List AbsensiKegiatan', 'url'=>array('index')),
	array('label'=>'Create AbsensiKegiatan', 'url'=>array('create')),
);*/


?>


<!-- COSTUMIZE -->




<h3>Presensi Kegiatan</h3>

<h4>
<?php echo $namakegiatan->nama_kegiatan; ?>
</h4>




<script type="text/javascript">
    function updateABlock(grid_id) {
		
        var keyId = $.fn.yiiGridView.getSelection(grid_id);
        keyId  = keyId[0]; //above function returns an array with single item, so get the value of the first item
 
        $.ajax({
            url: '<?php echo $this->createUrl('PartUpdate'); ?>',
            data: {id: keyId},
            type: 'GET',
            success: function(data) {
               // $('#part-block').html(data);
				$.fn.yiiGridView.update('presensi-kegiatan-grid');
            }
        });
    }
</script>


<?php



$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'presensi-kegiatan-grid',
	'dataProvider'=>$dataProvider,

	'filter'=>$model,
    'rowCssClassExpression'=>'($data->kehadiran=="yes")?"odd":"even"',
	'cssFile' => Yii::app()->request->baseUrl . '/css/gridview/styles.css',
	'selectableRows'=>'1',
	'selectionChanged' => 'updateABlock',
	'columns'=>array(
		
		'nim_mahasiswa',
		  array( // this is for your related group members of the current group
            'name'=>'nama_search', // this will access the attributeLabel from the member model class, and assign it to your column header
            'value'=>'$data->User["nama"]', // this will access the current group's 1st member and give out the firstname of that member
            'type'=>'raw', // this tells that the value type is raw and no formatting is to be applied to it
			'htmlOptions'=>array( 'width'=>'400' )

         ),
		array(
		'name'=>'kehadiran',
		'value'=>'($data->kehadiran=="yes")?CHtml::image(Yii::app()->baseUrl."/images/yes.png"):CHtml::image(Yii::app()->baseUrl."/images/no.png")',
		'type'=>'raw',
		'htmlOptions'=>array('style'=>'text-align: center'),

		),
	
	),
));


   ?>
  


