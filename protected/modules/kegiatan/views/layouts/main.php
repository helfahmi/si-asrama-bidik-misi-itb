<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<?php echo Yii::app()->bootstrap->register();?>
	
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/content.css" />
	
	<link rel="shortcut icon" href="http://www.fitb.itb.ac.id/saveourearth/wp-content/uploads/2013/04/itb-1920-small1.png"/>
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container">

	<div id="header">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header.png" alt="">

		<!--<div id="logo"><?php //echo CHtml::encode(Yii::app()->name); ?></div>-->
	</div><!-- header -->
	
		
			<div id="content_left">
				<div class="menu>
					<?php 
						if (Yii::app()->user->getState("role")=="tutor" || Yii::app()->user->getState("role")=="mahasiswa")
						{
							$this->widget('zii.widgets.CMenu',array(
								'items'=>array(
									array('label'=>'Kegiatan', 'url'=>array('/kegiatan'), 'visible'=>!Yii::app()->user->isGuest),
									array('label'=>'Ajukan Kegiatan', 'url'=>array('/kegiatan/kegiatan/form_ajuan'), 'visible'=>!Yii::app()->user->isGuest),
									array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
									array('label'=>'Change Password', 'url'=>array('/kegiatan/password/changepassword'), 'visible'=>!Yii::app()->user->isGuest),
									array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
									//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
								),
							)); 
						}
						else
						{
							$this->widget('zii.widgets.CMenu',array(
								'items'=>array(
									array('label'=>'Lihat Laporan', 'url'=>array('/kegiatan/laporan'), 'visible'=>!Yii::app()->user->isGuest),
									array('label'=>'Kegiatan', 'url'=>array('/kegiatan'), 'visible'=>!Yii::app()->user->isGuest),
									array('label'=>'Konfirmasi Ajuan', 'url'=>array('/kegiatan/kegiatan/konfirmasi'), 'visible'=>!Yii::app()->user->isGuest),
									array('label'=>'Buat Kegiatan', 'url'=>array('/kegiatan/kegiatan/buat'), 'visible'=>!Yii::app()->user->isGuest),
									array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
									array('label'=>'Change Password', 'url'=>array('/kegiatan/password/changepassword'), 'visible'=>!Yii::app()->user->isGuest),
									array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
									//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
								),
							)); 
						}
					?>
				</div>
			</div>
		<div id="content_right">
			<?php echo $content; ?>
		</div>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by AjeGILE.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
