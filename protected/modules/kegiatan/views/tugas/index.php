<?php
/* @var $this UploadTugasController */

$this->breadcrumbs=array(
	'Upload Tugas',
);
?>

<h2>Upload Tugas Kegiatan</h2>
<br>
	<p>Format Penamaan Tugas : <b>Tugas_Kegiatan_XX_NIM.pdf</b> dengan <b>XX</b> adalah nomor kegiatan </p>
<br>

<?php 
	$form = $this->beginWidget
	(
		'CActiveForm',
		array
		(
			'id' => 'upload-form',
			'enableAjaxValidation' => false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		)
	);
	
	$rows = Yii::app()->db->createCommand()->select()->from('kegiatan')->order('last_update desc')->limit(10)->queryAll();
	
	echo "<center>";
	echo $form->labelEx($model, '<h4> Uploader </h4>');
	echo "<br>";
	echo $form->fileField($model, 'file_name');
	echo $form->error($model, 'file_name');
	echo " <input type='hidden' value='$id' name='id'/> ";
	echo "<br><br>";
	echo CHtml::submitButton('Submit', array('submit'=>'Create'));
	
	echo "</center>";
	$this->endWidget();
?>
