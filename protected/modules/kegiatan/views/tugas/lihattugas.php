<?php 
	echo "<h2>Lihat Tugas Kegiatan ".$id." </h2>";
?>


<?php

	$this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider' => $dataProvider,
        'columns' => array(
		
		'id_tugas',
		'nim_mahasiswa',
		'nama_mahasiswa',
		'id_kegiatan',
		'status',
		'file_name',
		
        
		//specify the colums you wanted here
		array(
			'class'=>'CLinkColumn',		    
			'header'=>'File',		    
			'urlExpression'=>'Yii::app()->request->baseUrl."/uploads/tugas/".$data->file_name',			    
			'label'=>'Unduh',
        
		),
    ))
	);
?>
