<?php
/* @var $this KegiatanController */

$this->breadcrumbs=array(
	'Kegiatan',
);
?>
<h3>Buletin Kegiatan</h3><br>
	
<?php 
	$rows = Yii::app()->db->createCommand()->select()->from('kegiatan')->order('last_update desc')->limit(10)->queryAll();
	
	$rows = array_filter($rows);
	
	if(!empty($rows))
	{
		foreach($rows as $row)
		{
			//if(Yii::app()->user->getState("role")=="tutor")
			//{
				echo "<h4><a href='".Yii::app()->createUrl("/kegiatan/presensikegiatan/".$row['id_kegiatan'])."'>" . $row['nama_kegiatan'] . "</a></h4>";
		//	}
			/*else
			{
				echo "<h4>" . $row['nama_kegiatan'] . "</h4>";
			}*/
			echo "<h5>". $row['tgl_pelaksanaan'] . "</h5>";
			echo "<h5>Bertempat di " . $row['tempat_pelaksanaan'] . " dimulai pukul " . /*$row['waktu_mulai'] .*/ " WIB</h5>";
			echo "<p>" . $row['deskripsi_kegiatan'] . "</p>";
			$list = explode(",",$row['peserta']);
			echo "<p>Wajib bagi penghuni Asrama ";
			$i = 0;
			foreach($list as $id)
			{
				$asrama = Yii::app()->db->createCommand()->select()->from('asrama_id')->where('id_asrama=:id', array(':id'=>$id))->queryRow();
				if($i == 0)
				{
					echo $asrama['nama'];
				}
				else
				{
					echo ", ".$asrama['nama'];
				}
				$i++;
			}
			echo "</p>";
			
			if(Yii::app()->user->getState("role")=="mahasiswa")
			{
				echo "<h5><a href='".Yii::app()->createUrl("/kegiatan/tugas/".$row['id_kegiatan'])."'>Upload Tugas</a></h5>";
			}
			else
			{
				echo "<h5><a href='".Yii::app()->createUrl("/kegiatan/tugas/".$row['id_kegiatan'])."'>Lihat Tugas</a></h5>";
			}
			
			echo '<p><i>last update</i> : ' . $row['last_update'] . '</p><br><br>';		
		}
	}
	else
	{
		echo "<h5>Tidak ada kegiatan</h5>";
	}
?>
