<?php
/* @var $this KegiatanController */

$this->breadcrumbs=array(
	'Kegiatan'=>array('/kegiatan'),
	'Konfirmasi',
);
?>
<h3>Konfirmasi Ajuan Kegiatan</h3><br>
	
<?php 
	$rows = Yii::app()->db->createCommand()->select()->from('ajuan_kegiatan')->order('last_update asc')->queryAll();
	
	$rows = array_filter($rows);
	
	if(!empty($rows))
	{
		foreach($rows as $row)
		{
			echo "<h4>" . $row['nama_kegiatan'] . "</h4>";
			echo "<h5>". $row['tgl_pelaksanaan'] . "</h5>";
			echo "<h5>Bertempat di " . $row['tempat_pelaksanaan'] . " dimulai pukul " . $row['waktu_mulai'] . " WIB</h5>";
			echo "<p>" . $row['deskripsi_kegiatan'] . "</p>";
			echo "<p>Diajukan oleh : " . $row['pengaju'] . "</p>";
			echo '<p><i>last update</i> : ' . $row['last_update'] . '</p>';
			/*
			
			$this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'Konfirmasi',
			'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
			'size'=>'large', // null, 'large', 'small' or 'mini'
			));
			$this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'Tolak',
			'type'=>'danger', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
			'size'=>'large', // null, 'large', 'small' or 'mini'
			));
			*/
			echo "<h5><a href='" . Yii::app()->request->baseUrl . "/kegiatan/confirm/" . $row['id_kegiatan'] ."'>Konfirmasi</a></h5><br><br>";
		}
	}
	else
	{
		echo "<h5>Tidak ada ajuan kegiatan</h5>";
	}
?>
