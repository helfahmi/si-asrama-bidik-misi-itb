<?php
/* @var $this AjuanKegiatanController */
/* @var $model AjuanKegiatan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ajuan-kegiatan-form_ajuanKegiatan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>
		<h4>Form Pengajuan Kegiatan Pembinaan Asrama ITB</h4>

		<p class="note"><span class="required">*</span> wajib diisi</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_kegiatan'); ?>
		<?php echo $form->textField($model,'nama_kegiatan'); ?>
		<?php echo $form->error($model,'nama_kegiatan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_pelaksanaan'); ?>
		<?php                    
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'name'=>$model->tgl_pelaksanaan,
				'value'=>$model->tgl_pelaksanaan,
				'model'=>$model,
				'attribute'=>'tgl_pelaksanaan',
				// additional javascript options for the date picker plugin
				'options'=>array(
					'showAnim'=>'fold',
					'dateFormat'=>'yy-mm-dd',
					'altField'=>'#self_pointing_id',
					'altFormat'=>'dd-mm-yy',
				),
					'htmlOptions'=>array(
					'style'=>'height:20px;'
				),
			));                         
		?> 
		<?php echo $form->error($model,'tgl_pelaksanaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tempat_pelaksanaan'); ?>
		<?php echo $form->textField($model,'tempat_pelaksanaan'); ?>
		<?php echo $form->error($model,'tempat_pelaksanaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'waktu_mulai'); ?>
		<?php //echo $form->textField($model,'waktu_mulai'); ?>
		<?php 
			$this->widget('ext.timepicker.EJuiDateTimePicker', array(
			'model'=>$model,
			'name'=>$model->waktu_mulai,
			'value'=>$model->waktu_mulai,
			'attribute'=>'waktu_mulai',
			'options' => array(
                'showOn'=>'focus',
                'timeFormat'=>'hh:mm:ss',
                'timeOnly'=>true,
			),
			'htmlOptions' => array(
                'style'=>'width:150px;', // styles to be applied
                'maxlength' => '10',    // textField maxlength
			),
			));
		?>
		<?php echo $form->error($model,'waktu_mulai'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deskripsi_kegiatan'); ?>
		<?php echo $form->textField($model,'deskripsi_kegiatan'); ?>
		<?php echo $form->error($model,'deskripsi_kegiatan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'peserta'); ?>
		<?php
			$rows = Yii::app()->db->createCommand()->select()->from('asrama_Id')->queryAll();
			$counter=0;
			foreach($rows as $row)
			{
				echo "<input type='checkbox' name='".$counter."' value='".$row['id_asrama']."'/>".$row['nama']."</br>";
				$counter++;
			}
			echo "<input type='hidden' name='counter' value='".$counter."'/>";
		?>
		<?php //echo $form->checkBoxList($model,'asramaIds',CHtml::listData(asramaId::model()->findAll(),'id_asrama','nama'),array('peserta' => 'id')); ?>
		<?php echo $form->error($model,'peserta'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->