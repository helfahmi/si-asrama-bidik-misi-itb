<?php
/* @var $this MahasiswaController */
/* @var $model Mahasiswa */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mahasiswa-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nim_mahasiswa'); ?>
		<?php echo $form->textField($model,'nim_mahasiswa',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'nim_mahasiswa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_mahasiswa'); ?>
		<?php echo $form->textField($model,'nama_mahasiswa',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'nama_mahasiswa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'letak_asrama'); ?>
		<?php echo $form->textField($model,'letak_asrama',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'letak_asrama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_kamar'); ?>
		<?php echo $form->textField($model,'no_kamar'); ?>
		<?php echo $form->error($model,'no_kamar'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->