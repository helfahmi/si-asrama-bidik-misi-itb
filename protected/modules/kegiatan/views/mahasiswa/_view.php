<?php
/* @var $this MahasiswaController */
/* @var $data Mahasiswa */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('nim_mahasiswa')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->nim_mahasiswa), array('view', 'id'=>$data->nim_mahasiswa)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_mahasiswa')); ?>:</b>
	<?php echo CHtml::encode($data->nama_mahasiswa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('letak_asrama')); ?>:</b>
	<?php echo CHtml::encode($data->letak_asrama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kamar')); ?>:</b>
	<?php echo CHtml::encode($data->no_kamar); ?>
	<br />


</div>