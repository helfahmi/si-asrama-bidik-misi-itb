<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<?php echo Yii::app()->bootstrap->register();?>
	
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	
	<link rel="shortcut icon" href="http://www.fitb.itb.ac.id/saveourearth/wp-content/uploads/2013/04/itb-1920-small1.png"/>
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container">

	<div id="header">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/header.png" alt="">
		
		<!--<div id="logo"><?php //echo CHtml::encode(Yii::app()->name); ?></div>-->
	</div><!-- header -->
	
	<div class="container">
		<!-- COSTUMIZE -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/content.css" />
		<br/>
		<div id="content_left">
			<ul class="menu">
				<li>
					<a href="<?php echo $this->createUrl('pembayaran/index'); ?>" class="navigation">Dashboard</a>
				</li>
				<?php if(Yii::app()->user->getState('nid') != 0){ //Admin does not need to pay ?>
					<li>
						<a href="<?php echo $this->createUrl('pembayaran/status'); ?>" class="navigation">Histori Pembayaran</a>
					</li>				
					<li>
						<a href="<?php echo $this->createUrl('pembayaran/create'); ?>" class="navigation">Tambah Pembayaran Baru</a>
					</li>
				<?php } 
				if(Yii::app()->user->getState('nid') == 0){ ?>
					<li>
						<a href="<?php echo $this->createUrl('pembayaran/verify'); ?>" class="navigation">Verifikasi</a>
					</li>
					<li>
						<a href="<?php echo $this->createUrl('pembayaran/laporan'); ?>" class="navigation">Lihat Laporan</a>
					</li>
					<li>
						<a href="<?php echo $this->createUrl('pembayaran/manageuser'); ?>" class="navigation">Manage User</a>
					</li>
				<?php } ?>
				<li>
					<a href="<?php echo $this->createUrl('site/logout'); ?>" class="navigation">Logout</a>
				</li>
			</ul>
		</div>
		<div id="content_right">
			<?php echo $content; ?>
		</div>
	</div>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by AjeGILE.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
