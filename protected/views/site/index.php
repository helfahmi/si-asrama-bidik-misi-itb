<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<ul>
	<li><a href="/pendaftaran">Sistem Informasi Pendaftaran</a></li>
	<li><a href="<?php echo Yii::app()->createUrl('kegiatan'); ?>">Sistem Informasi Kegiatan</a></li>
	<li><a href="<?php echo Yii::app()->createUrl('pembayaran'); ?>">Sistem Informasi Keuangan</a></li>
</ul>