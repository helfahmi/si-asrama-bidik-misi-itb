<h1>Manage User</h1>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
	'dataProvider'=>$dataProvider,
	'template'=>"{pager}{items}{pager}",
	'columns'=>array(
		//array('name'=>'user', 'header'=>'Username'),
		array('name'=>'NID', 'header'=>'NID'),
		array('name'=>'nama', 'header'=>'Nama'),
		array('name'=>'email', 'header'=>'E-mail'),
		array('name'=>'tanggal_mulai_bayar', 'header'=>'Tanggal Mulai Bayar', 'value'=>'$data->tanggalMulaiValue'),
		array(
			'header'=>'Control',
			//'value'=>'"woi"',
			'value'=>'$data->editTanggalMulaiButton',
			'type'=>'raw',
			//'htmlOptions'=>array('style'=>'width: 50px'),
			//'template'=>'{view}' //removed {view}
		),
	),
)); ?>