<h1>Pembayaran</h1>
	<?php $this->widget('bootstrap.widgets.TbGridView', array(
		'type'=>'striped bordered condensed',
		'dataProvider'=>$dataProvider,
		'template'=>"{items}",
		'columns'=>array(
			array('name'=>'user', 'header'=>'Username'),
			array('name'=>'nominal', 'header'=>'Nominal', 'value'=>'$data->formattedNominal'),
			array('name'=>'tanggal', 'header'=>'Tanggal', 'value'=>'$data->formattedDate'),
			array('name'=>'bukti', 'header'=>'Bukti', 'value'=>'$data->formattedBukti', 'type'=>'raw'),
			array('name'=>'status', 'header'=>'Status Konfirmasi'),
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'htmlOptions'=>array('style'=>'width: 50px'),
			),
		),
	)); ?>