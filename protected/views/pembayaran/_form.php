<br/>
<?php $this->widget('bootstrap.widgets.TbButton', array(
	'type'=>'action',
	'encodeLabel'=>false,
	'label'=>'<span class="icon icon-chevron-left"></span> Kembali',
	'url'=>array('pembayaran/index'),
)); ?>
		
<h1><?php echo $title; ?></h1>

<div class="alert alert-info">Pembayaran Anda akan masuk kepada bulan terakhir yang belum Anda bayar (<?php echo date("F Y", strtotime(MulaiBayar::model()->lastUnpaidMonth(Yii::app()->user->nid))); ?>). </div>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pembayaran-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->textFieldRow($model,'nominal', array('prepend'=>'Rp'), array('class'=>'span5')); ?>
 <?php echo $form->fileFieldRow($model, 'bukti'); ?>
 
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'encodeLabel'=>false,
		'label'=>$model->isNewRecord ? '<span class="icon icon-plus"></span> Buat Baru' : '<span class="icon icon-repeat"></span> Simpan',
	)); ?>
</div>

<?php $this->endWidget(); ?>