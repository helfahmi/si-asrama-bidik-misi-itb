<?php
/* @var $this MulaiBayarController */
/* @var $model MulaiBayar */
/* @var $form CActiveForm */
?>
<h1>Edit Tanggal Mulai Bayar</h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mulai-bayar-edit_bayar-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_user'); ?>
		<?php echo $form->textField($model,'id_user', array('disabled'=>'true')); ?>
		<?php echo $form->error($model,'id_user'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tanggal_mulai_bayar'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model'=>$model,
				'attribute'=>'tanggal_mulai_bayar',
				//'flat'=>true,//remove to hide the datepicker
				'options'=>array(
					'dateFormat' => 'yy-mm-dd',
					//'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
				),
				'htmlOptions'=>array(
				),
			));
		?>
		<?php //echo $form->textField($model,'tanggal_mulai_bayar'); ?>
		<?php echo $form->error($model,'tanggal_mulai_bayar'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->