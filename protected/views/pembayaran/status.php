<h1>Pembayaran</h1>

Biaya bulanan yang harus dibayar sejak: <?php echo date("d F Y", strtotime(MulaiBayar::model()->findByAttributes(array('id_user'=>Yii::app()->user->nid))->tanggal_mulai_bayar)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
	'dataProvider'=>$dataProvider,
	'template'=>"{pager}{items}{pager}",
	'columns'=>array(
		//array('name'=>'user', 'header'=>'Username'),
		array('name'=>'tanggal', 'header'=>'Tanggal', 'value'=>'$data->formattedDate'),
		array('name'=>'bulan_pembayaran', 'header'=>'Bulan Dibayar', 'value'=>'$data->formattedBayarDate'),
		array('name'=>'nominal', 'header'=>'Nominal', 'value'=>'$data->formattedNominal'),
		array('name'=>'bukti', 'header'=>'Bukti', 'value'=>'$data->formattedBukti', 'type'=>'raw'),
		array('name'=>'status', 'header'=>'Status Konfirmasi'),
		array('name'=>'keterangan', 'header'=>'Keterangan'),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view}{delete}' //removed {view}
		),
	),
)); ?>