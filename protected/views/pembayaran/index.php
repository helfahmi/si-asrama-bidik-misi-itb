<h3>Halaman Pembayaran</h3><br/>

Selamat datang, <b><?php echo Yii::app()->user->getState('nama'); ?></b>. <br/><br/>

<?php
	if(Yii::app()->user->nid != 0){	
		echo "Status pembayaran Anda bulan ini: ";
		//echo "NID: " . Yii::app()->user->getState('nid')."<br/>";
		
		$bulan_bayars = Pembayaran::model()->findAll("user=:nid", array(':nid' => Yii::app()->user->getState('nid')));
		//Pembayaran::model()->findByAttributes(array("user"=>Yii::app()->user->getState('nid')));
		$found = false;
		foreach($bulan_bayars as $bulan_bayar){
			//print_r($bulan_bayar);
			//echo date("m-Y"). " - ". date("m-Y", strtotime($bulan_bayar->bulan_pembayaran))." - ".$bulan_bayar->bulan_pembayaran."<br/>";
			if(date("m-Y") == date("m-Y", strtotime($bulan_bayar->bulan_pembayaran))){
				if($bulan_bayar->status == "Belum"){
					echo "<b style='color:yellow;'>Belum dikonfirmasi.</b><br/>";
				} else {//Sudah
					echo "<b style='color:green;'>Sudah membayar.</b><br/>";
				}
				$found = true;
			}	
		}
		if(!$found){
			echo "<b style='color:red;'>Belum membayar</b>.<br/>";
		}
	}
?>