<h1>Pembayaran</h1>
	
	<?php 
	
	$this->widget('bootstrap.widgets.TbGridView', array(
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'template'=>"{pager}{items}{pager}",
		'columns'=>array(
			array('name'=>'user', 'header'=>'Username'),
			array('name'=>'nominal', 'header'=>'Nominal', 'value'=>'$data->formattedNominal'),
			array('name'=>'tanggal', 'header'=>'Tanggal', 'value'=>'$data->formattedDate'),
			array('name'=>'bulan_pembayaran', 'header'=>'Bulan Dibayar', 'value'=>'$data->formattedBayarDate'),
			array('name'=>'bukti', 'header'=>'Bukti', 'value'=>'$data->formattedBukti', 'type'=>'raw'),
			array(
				'class' => 'editable.EditableColumn',
				'name'=>'status', 
				'header'=>'Status Konfirmasi',
				'editable' => array(
					'type'		 => 'select',
					'url'        => $this->createUrl('updateEditable', array('model'=>'Pembayaran', 'field'=>'status')),
					'placement'  => 'top',
					'source'    => Editable::source(array('Belum'=>'Belum', 'Sudah'=>'Sudah')),
					'inputclass' => 'span3',
				)				
			),
			array( 
				'class' => 'editable.EditableColumn',
				'name' => 'keterangan',
				//'value' => 'CHtml::value($data, "profile.language")',
				'editable' => array(
					'type'      => 'text',
					'attribute' => 'keterangan',
					'url'       => $this->createUrl('updateEditable', array('model'=>'Pembayaran', 'field'=>'status')),
					'placement' => 'left',
				)
			), 
		),
	)); ?>