<h2>Laporan Pembayaran - <?php echo date("F",strtotime("14-".$month."-01"))." ".$year?></h2>
	<?php $url=CHtml::normalizeUrl(array(Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId()));?>
	<?php $bulan = date('m', strtotime($year."-".$month."-01 -1 month"));
		$tahun = date('Y', strtotime($year."-".$month."-01 -1 month"));
		$this->widget('bootstrap.widgets.TbButton', array(
		'label'=>'<<Bulan Sebelumnya',
		'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
		'size'=>'small', // null, 'large', 'small' or 'mini'
		'url'=>$url."?month=".$bulan."&year=".$tahun,
	)); ?>
	<?php if (($year<date("Y"))||($year==date("Y")&&($month<date("m")))) {
		$bulan = date('m', strtotime($year."-".$month."-01 +1 month"));
		$tahun = date('Y', strtotime($year."-".$month."-01 +1 month"));
		$this->widget('bootstrap.widgets.TbButton', array(
		'label'=>'Bulan Berikutnya>>',
		'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
		'size'=>'small', // null, 'large', 'small' or 'mini'
		'url'=>$url."?month=".$bulan."&year=".$tahun,));
		} ?>
	
	<h3>Statistik</h3>
	<?php $this->widget('bootstrap.widgets.TbDetailView', array(
		'type'=>'bordered',
		'data'=>array('sbst'=>$nSudahBayarSudahDiverifikasi, 'sbbt'=>$nSudahBayarBelumDiverifikasi, 'bb'=>$nBelumBayar),
		'attributes'=>array(
			array('name'=>'sbst', 'label'=>'Sudah Membayar, Sudah Terkonfirmasi'),
			array('name'=>'sbbt', 'label'=>'Sudah Membayar, Belum Terkonfirmasi'),
			array('name'=>'bb', 'label'=>'Belum Membayar'),
		),
	)); ?>	
	
	<?php $this->widget('bootstrap.widgets.TbGridView', array(
		'type'=>'striped bordered condensed',
		'dataProvider'=>$dataProvider,
		'template'=>"{pager}{items}{pager}",
		'columns'=>array(
			array('name'=>'nim', 'header'=>'NIM Penghuni'),
			array('name'=>'nama', 'header'=>'Nama Penghuni'),
			array('name'=>'status', 'header'=>'Status Pembayaran', 'value'=>'getStatus($data["status"])'),
			array('name'=>'tanggal', 'header'=>'Tanggal Pembayaran', 'value'=>'getTanggal($data["tanggal"])'),
		),
	)); ?>
	
	<?php
		function getStatus ($status) {
			if (!(isset($status))) {
				return "Belum membayar";
			}
			else if ($status=="Belum") {
				return "Sudah membayar, belum diverifikasi";
			}
			else {
				return "Sudah membayar, sudah diverivikasi";
			}
		}
		function getTanggal($tanggal) {
			if (!(isset($tanggal))) {
				return "---------------";
			}
			else {
				return $tanggal;
			}
		}
	?>