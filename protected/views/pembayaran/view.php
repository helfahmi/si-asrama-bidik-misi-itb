<?php $this->widget('bootstrap.widgets.TbButton', array(
	'type'=>'action',
	'encodeLabel'=>false,
	'label'=>'<span class="icon icon-chevron-left"></span> Kembali',
	'url'=>array('pembayaran/index'),
)); ?>

<h1>View</h1>
	
	<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        array('name'=>'user', 'label'=>'Username'),
        array('name'=>'nominal', 'label'=>'Nominal'),
        array('name'=>'tanggal', 'label'=>'Tanggal Pembayaran', 'value'=>$model->formattedDate),
		array('name'=>'bulan_bayar', 'label'=>'Bulan Pembayaran', 'value'=>$model->formattedBayarDate),
		array('name'=>'bukti', 'label'=>'Bukti Pembayaran'),
		array('name'=>'keterangan', 'label'=>'Keterangan'),
	),
)); ?>

	<?php /* $this->widget('bootstrap.widgets.TbButton', array(
		'label'=>'Edit',
		'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
		'size'=>'normal', // null, 'large', 'small' or 'mini'
		'url'=>array('update', 'id'=>$model->id),
	)); */?>
